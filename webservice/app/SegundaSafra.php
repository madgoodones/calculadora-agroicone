<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class SegundaSafra extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'cultura',
		'area_produtiva',
		'produtividade_media_3_safras',
		'custo_producao',
		'preco_recebido',
		'producao_atual'
	];
}
