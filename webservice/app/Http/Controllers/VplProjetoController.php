<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\VplProjeto;
use CalculadoraAgroicone\Http\Requests\VplProjetosRequest;

class VplProjetoController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.vpl-projeto.viewVplProjeto');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vplProjetos = VplProjeto::all();
        return response()->json([
            'vplProjetos' => $vplProjetos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VplProjetosRequest $request)
    {
        $vplProjeto = VplProjeto::create($request->all());
        return response()->json([
            'message' => 'Taxa criada com sucesso',
            'vplProjeto' => $vplProjeto
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VplProjetosRequest $request, $id)
    {
        $vplProjeto = VplProjeto::find($id);
        $vplProjeto->fill($request->all());
        $vplProjeto->update();
        return response()->json([
            'message' => 'Taxa atualizada com sucesso',
            'vplProjeto' => $vplProjeto
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        VplProjeto::find($id)->delete();
        return response()->json([
            'message' => 'Taxa excluído com sucesso'
        ]);
    }
}
