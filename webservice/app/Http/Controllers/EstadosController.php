<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\Estado;

class EstadosController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.estados.viewEstados');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = Estado::all();
        return response()->json([
            'estados' => $estados
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|min:3',
            'uf' => 'required|min:2|max:3'
        ]);
        $estado = Estado::create($request->all());
        return response()->json([
            'message' => 'Estado criado com sucesso',
            'estado' => $estado
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required|min:3',
            'uf' => 'required|min:2|max:3'
        ]);
        $estado = Estado::find($id);
        $estado->fill($request->all());
        $estado->update();
        return response()->json([
            'message' => 'Estado atualizado com sucesso',
            'estado' => $estado
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Estado::find($id)->delete();
        return response()->json([
            'message' => 'Estado excluído com sucesso'
        ]);
    }
}
