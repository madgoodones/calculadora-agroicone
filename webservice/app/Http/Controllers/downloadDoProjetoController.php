<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;

use CalculadoraAgroicone\Mail\downloadDoProjetoMail;
use CalculadoraAgroicone\Mail\enviarProjetoMail;
use Mail;

class downloadDoProjetoController extends Controller
{
	public function downloadDoProjeto(Request $request){
		$this->validate($request, [
            'nome' => 'required|min:3',
            'email' => 'required|email'
        ]);
		if (empty($request->projeto)) {
			Mail::to('joel.santos@madgo.com.br')
			->send(new downloadDoProjetoMail($request));
			return response()->json(
				[
					'sucesso' => 'Tudo certo!'
				],
				200
			);
		} else {
			Mail::to($request->email, $request->nome)
			->send(new enviarProjetoMail($request));
			return response()->json(
				[
					'sucesso' => 'Tudo certo...enviado para o seu e-mail!'
				],
				200
			);
		}
	}
}
