<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\CustosPadroes;
use CalculadoraAgroicone\Http\Requests\CustosPadroesRequest;

class CustosPadroesController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.custos-padrao.viewCustosPadrao');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $custosPadrao = CustosPadroes::all();
        return response()->json([
            'custosPadrao' => $custosPadrao
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustosPadroesRequest $request)
    {
        $custoPadrao = CustosPadroes::create($request->all());
        return response()->json([
            'message' => 'Custo Padrão criado com sucesso',
            'custoPadrao' => $custoPadrao
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustosPadroesRequest $request, $id)
    {
        $custoPadrao = CustosPadroes::find($id);
        $custoPadrao->fill($request->all());
        $custoPadrao->update();
        return response()->json([
            'message' => 'Custo Padrão atualizado com sucesso',
            'custoPadrao' => $custoPadrao
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CustosPadroes::find($id)->delete();
        return response()->json([
            'message' => 'Custo Padrão excluído com sucesso'
        ]);
    }
}
