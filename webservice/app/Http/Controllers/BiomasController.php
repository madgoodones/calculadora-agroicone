<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\Bioma;
use CalculadoraAgroicone\Http\Requests\BiomasRequest;

class BiomasController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.biomas.viewBiomas');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $biomas = Bioma::all();
        return response()->json([
            'biomas' => $biomas
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BiomasRequest $request)
    {
        $bioma = Bioma::create($request->all());
        return response()->json([
            'message' => $bioma->nome . ' criado com sucesso',
            'bioma' => $bioma
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BiomasRequest $request, $id)
    {
        $bioma = Bioma::find($id);
        $bioma->fill($request->all());
        $bioma->update();
        return response()->json([
            'message' => $bioma->nome . ' atualizado com sucesso',
            'bioma' => $bioma
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Bioma::find($id)->delete();
        return response()->json([
            'message' => 'Bioma excluído com sucesso'
        ]);
    }
}
