<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\CustosRestauro;
use CalculadoraAgroicone\Http\Requests\CustosRestaurosRequest;

class CustosRestaurosController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.custos-restauros.viewCustosRestauros');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $custosRestauro = CustosRestauro::all();
        return response()->json([
            'custosRestauros' => $custosRestauro
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustosRestaurosRequest $request)
    {
        $custoRestauro = CustosRestauro::create($request->all());
        return response()->json([
            'message' => $custoRestauro->nome . ' criado com sucesso',
            'custoRestauro' => $custoRestauro
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustosRestaurosRequest $request, $id)
    {
        $custoRestauro = CustosRestauro::find($id);
        $custoRestauro->fill($request->all());
        $custoRestauro->update();
        return response()->json([
            'message' => $custoRestauro->nome . ' atualizado com sucesso',
            'custoRestauro' => $custoRestauro
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CustosRestauro::find($id)->delete();
        return response()->json([
            'message' => 'Custo de Restauro excluído com sucesso'
        ]);
    }
}
