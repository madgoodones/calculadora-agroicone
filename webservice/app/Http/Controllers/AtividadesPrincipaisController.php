<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\AtividadesPrincipais;

class AtividadesPrincipaisController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.atividades-principais.viewAtividadesPrincipais');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $atividadesPrincipais = AtividadesPrincipais::all();
        return response()->json([
            'atividadesPrincipais' => $atividadesPrincipais
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nome' => 'required|min:3'
        ]);
        $atividadePrincipal = AtividadesPrincipais::create($request->all());
        return response()->json([
            'message' => $atividadePrincipal->nome . ' criada com sucesso',
            'atividadePrincipal' => $atividadePrincipal
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nome' => 'required|min:3'
        ]);
        $atividadePrincipal = AtividadesPrincipais::find($id);
        $atividadePrincipal->fill($request->all());
        $atividadePrincipal->update();
        return response()->json([
            'message' => $atividadePrincipal->nome . ' atualizada com sucesso',
            'atividadePrincipal' => $atividadePrincipal
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        AtividadesPrincipais::find($id)->delete();
        return response()->json([
            'message' => 'Atividade excluída com sucesso'
        ]);
    }
}
