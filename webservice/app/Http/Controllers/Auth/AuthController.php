<?php

namespace CalculadoraAgroicone\Http\Controllers\Auth;

use Illuminate\Http\Request;
use CalculadoraAgroicone\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
  /**
   * Handle a registration request for the application.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function registerUser(Request $request)
  {
      $this->validator($request->all())->validate();

      event(new Registered($user = $this->create($request->all())));

      $this->guard()->login($user);

      return $this->registered($request, $user)
                      ?: redirect($this->redirectPath());
  }
  /**
   * Show the application registration form.
   *
   * @return \Illuminate\Http\Response
   */
  public function showFrom()
  {
      return view('auth.register');
  }
}
