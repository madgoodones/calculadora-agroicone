<?php

namespace CalculadoraAgroicone\Http\Controllers\Auth;

use CalculadoraAgroicone\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Auth;
use Session;
use CalculadoraAgroicone\User;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    /**
     * Logout de usuario
     * @return route "redireciona para uma pagina"
     */
    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

    public function authApi(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $usuario = new User();
        $sql = DB::select('select id, password from users where email = ?', [$request->email]);
        if(!empty($sql))
        {
            $passwordTemp = $sql[0]->password;
            if (Hash::check($request->password, $passwordTemp))
            {
                $usuario = $usuario->findOrFail($sql[0]->id);
                return response(["status" => "4"], 200)->header('Content-Type', 'application/json');
            }
             return response(["status" => "Senha inválida."], 401)->header('Content-Type', 'application/json');
        }
        return response(["status" => "E-mail não foi encontrado em nossa base de dados."], 401)->header('Content-Type', 'application/json');
    }
}
