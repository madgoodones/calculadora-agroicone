<?php

namespace CalculadoraAgroicone\Http\Controllers\Pages;

use Illuminate\Http\Request;
use CalculadoraAgroicone\Http\Controllers\Controller;

class PublicController extends Controller
{
    /**
     * Show the application public.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('public.home');
    }
}
