<?php

namespace CalculadoraAgroicone\Http\Controllers\Pages;

use Illuminate\Http\Request;
use CalculadoraAgroicone\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function view()
    {
        return view('dashboard.home');
    }
}
