<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\LinhasFinanciamento;
use CalculadoraAgroicone\Http\Requests\LinhasFinanciamentosRequest;

class LinhasFinanciamentosController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.linhas-financiamentos.viewLinhasFinanciamentos');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $linhas = LinhasFinanciamento::all();
        return response()->json([
            'linhasFinanciamentos' => $linhas
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LinhasFinanciamentosRequest $request)
    {
        $linhaFinanciamento = LinhasFinanciamento::create($request->all());
        return response()->json([
            'message' => $linhaFinanciamento->nome . ' criada com sucesso',
            'linhaFinanciamento' => $linhaFinanciamento
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(LinhasFinanciamentosRequest $request, $id)
    {
        $linhaFinanciamento = LinhasFinanciamento::find($id);
        $linhaFinanciamento->fill($request->all());
        $linhaFinanciamento->update();
        return response()->json([
            'message' => $linhaFinanciamento->nome . ' atualizada com sucesso',
            'linhaFinanciamento' => $linhaFinanciamento
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LinhasFinanciamento::find($id)->delete();
        return response()->json([
            'message' => 'Linha de financiamento excluída com sucesso'
        ]);
    }
}
