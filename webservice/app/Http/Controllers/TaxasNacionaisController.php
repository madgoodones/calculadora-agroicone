<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\TaxasNacionais;
use CalculadoraAgroicone\Http\Requests\TaxasNacionaisRequest;

class TaxasNacionaisController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.taxas-nacionais.viewTaxasNacionais');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taxasNacionais = TaxasNacionais::all();
        return response()->json([
            'taxasNacionais' => $taxasNacionais
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TaxasNacionaisRequest $request)
    {
        $taxaNacional = TaxasNacionais::create($request->all());
        return response()->json([
            'message' => 'Taxa criada com sucesso',
            'taxaNacional' => $taxaNacional
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TaxasNacionaisRequest $request, $id)
    {
        $taxaNacional = TaxasNacionais::find($id);
        $taxaNacional->fill($request->all());
        $taxaNacional->update();
        return response()->json([
            'message' => 'Taxa atualizada com sucesso',
            'taxaNacional' => $taxaNacional
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TaxasNacionais::find($id)->delete();
        return response()->json([
            'message' => 'Taxa excluído com sucesso'
        ]);
    }
}
