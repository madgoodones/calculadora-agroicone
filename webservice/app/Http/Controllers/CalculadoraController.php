<?php
namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;

use CalculadoraAgroicone\PremissasRegularizacoes;
use CalculadoraAgroicone\DadosPropriedade;
use CalculadoraAgroicone\UsoAtualAreaRegularizacoes;
use CalculadoraAgroicone\SegundaSafra;
use CalculadoraAgroicone\CustosFinanceiros;
use CalculadoraAgroicone\Estado;
use CalculadoraAgroicone\TaxasNacionais;

use CalculadoraAgroicone\Services\CalculadoraService;

class CalculadoraController extends Controller
{
	public function validarDados($dados) {

		$erros = [];

		// Validar Premissas
		$premissas = $dados['premissas_regularizacao'];
		if (!isset($premissas['estado']) || $premissas['estado'] <= 0) array_push($erros, 'Preencher o campo Estado em Área para Regularização.');
		if (!isset($premissas['bioma']) || $premissas['bioma'] <= 0) array_push($erros, 'Preencher o campo Bioma em Área para Regularização.');
		if (!isset($premissas['atividade']) || $premissas['atividade'] <= 0) array_push($erros, 'Preencher o campo Uso da área em Área para Regularização.');

		// Validar Atividade Principal vulgo Uso Atual da Área para Regularização
		$principal = $dados['uso_atual_area_regularizacao'];
		if (!isset($principal['area_produtiva']) || $principal['area_produtiva'] <= 0) array_push($erros, 'Preencher o campo Área Produtiva em Atividade Usada para a Regularização.');
		if (!isset($principal['custo_producao']) || $principal['custo_producao'] <= 0) array_push($erros, 'Preencher o campo Custo da Produção em Atividade Usada para a Regularização.');
		if (!isset($principal['preco_recebido']) || $principal['preco_recebido'] <= 0) array_push($erros, 'Preencher o campo Preço Recebido em Atividade Usada para a Regularização.');
		if (!isset($principal['produtividade_media_3_safras'])) array_push($erros, 'Preencher o campo Produtividade Média Últimas 3 Safras em Atividade Usada para a Regularização.');
		if (isset($premissas['atividade']) && $premissas['atividade'] == 4) {
			$principal['producao_atual'] = 0;
			if (!isset($principal['media_arrobas_animal']) || $principal['media_arrobas_animal'] <= 0) array_push($erros, 'Preencher o campo Média de Arrobas por Animal Vendido em Atividade Usada para a Regularização.');
			if (!isset($principal['animais_vendidos']) || $principal['animais_vendidos'] <= 0) array_push($erros, 'Preencher o campo Número de Animais Vendidos em Atividade Usada para a Regularização.');
			if (isset($principal['producao_atual'])) $principal['producao_atual'] = 0;
		} else {
			if (isset($principal['media_arrobas_animal'])) $principal['media_arrobas_animal'] = 0;
			if (isset($principal['animais_vendidos'])) $principal['animais_vendidos'] = 0;
			if (!isset($principal['producao_atual']) || $principal['producao_atual'] <= 0) array_push($erros, 'Preencher o campo Produção Atual em Atividade Usada para a Regularização.');
		}
		
		// Validar Segunda Safra se optado
		$secundaria = $dados['segunda_safra'];
		if ($secundaria['cultura'] != 0 && $premissas['atividade'] != 4 && $premissas['atividade'] != 2) {
			if (!isset($secundaria['area_produtiva']) || $secundaria['area_produtiva'] <= 0) array_push($erros, 'Preencher o campo Área Produtiva em Atividade Usada para a Regularização.');
			if (!isset($secundaria['produtividade_media_3_safras']) || $secundaria['produtividade_media_3_safras'] <= 0) array_push($erros, 'Preencher o campo Produtividade Média Últimas 3 Safras em Atividade Usada para a Regularização.');
			if (!isset($secundaria['custo_producao']) || $secundaria['custo_producao'] <= 0) array_push($erros, 'Preencher o campo Custo da Produção em Atividade Usada para a Regularização.');
			if (!isset($secundaria['preco_recebido']) || $secundaria['preco_recebido'] <= 0) array_push($erros, 'Preencher o campo Preço Recebido em Atividade Usada para a Regularização.');
			if (!isset($secundaria['producao_atual']) || $secundaria['producao_atual'] <= 0) array_push($erros, 'Preencher o campo Produção Atual em Atividade Usada para a Regularização.');
		} else {
			$secundaria['area_produtiva'] = 0;
			$secundaria['produtividade_media_3_safras'] = 0; 
			$secundaria['custo_producao'] = 0;
			$secundaria['preco_recebido'] = 0;
			$secundaria['producao_atual'] = 0;
		}

		// Validar Custos Financeiros
		$financeiros = $dados['custos_financeiros'];
		if (!isset($financeiros['juros_financiamento']) || $financeiros['juros_financiamento'] < 0) array_push($erros, 'Preencher o campo Juros em Custos Financeiros.');
		if (!isset($financeiros['tributo_itr_csr_funrural']) || $financeiros['tributo_itr_csr_funrural'] < 0) array_push($erros, 'Preencher o campo Tributo (ITR + CSR + FUNRURAL) em Custos Financeiros.');
		return $erros;
	}
	/**
	 * Organiza os dados e gera o resultado
	 * @return [Projeto] Projeto final de regularização
	 */
	public function calculate(Request $request) {
		$erros = $this->validarDados($request->all());
		if (sizeof($erros) > 0) {
			return response()->json(['erros' => $erros], 406);
		}
		// Input
		$premissasRegularizacao = new PremissasRegularizacoes;
		$premissasRegularizacao->fill($request->premissas_regularizacao);
		$dadosPropriedade = new DadosPropriedade;
		$dadosPropriedade->fill($request->dados_propriedade);
		$atividadePrincipal = new UsoAtualAreaRegularizacoes;
		$atividadePrincipal->fill($request->uso_atual_area_regularizacao);
		// CHANGE
		$atividadePrincipal->producao_atual = $atividadePrincipal->area_produtiva * $atividadePrincipal->produtividade_media_3_safras;
		$atividadeSecundaria = new SegundaSafra;
		$atividadeSecundaria->fill($request->segunda_safra);
		// CHANGE
		$atividadeSecundaria->producao_atual = $atividadeSecundaria->area_produtiva * $atividadeSecundaria->produtividade_media_3_safras;
		$custosFinanceiros = new CustosFinanceiros;
		$custosFinanceiros->fill($request->custos_financeiros);
		// Modelos
		$service = new CalculadoraService;

		$estado = Estado::findOrFail($premissasRegularizacao->estado);
		$taxasNacionais = TaxasNacionais::first();
		$bioma = $estado->biomas->where('id', $premissasRegularizacao->bioma)->first();
		/**
		 * Calcule Started
		 */
		// Regra de negócio do projeto
		if ($dadosPropriedade->deficit_rl < $dadosPropriedade->area_marginal) $dadosPropriedade->area_marginal = 0;
		if ($dadosPropriedade->deficit_rl < $dadosPropriedade->app_atual_conservada) $dadosPropriedade->deficit_rl = 0;
		else $dadosPropriedade->deficit_rl = $dadosPropriedade->deficit_rl - $dadosPropriedade->app_atual_conservada;

		// Atualiza valores conforme a atividade selecionada
		if ($premissasRegularizacao->atividade == 4) { // Se for Pecuária de Corte
			$atividadePrincipal->producao_atual = 0;
			$atividadePrincipal->fill($this->atualizaValoresAtividade($premissasRegularizacao->atividade, $atividadePrincipal, $atividadePrincipal->media_arrobas_animal, $atividadePrincipal->animais_vendidos));
		} else {
			// Atualiza valores do Uso Atual para Regularizacao
			$atividadePrincipal->fill($this->atualizaValoresAtividade($premissasRegularizacao->atividade, $atividadePrincipal));
			$atividadePrincipal->animais_vendidos = 0;
			$atividadePrincipal->media_arrobas_animal = 0;
			// Verifica se possui Segunda Safra
			if($atividadeSecundaria->cultura != 0) $atividadeSecundaria->fill($this->atualizaValoresAtividade($atividadeSecundaria->cultura, $atividadeSecundaria));
		}

		// Array com simulação de 20 Anos de Produtividade Media das últimas 3 safras
		$produtividade20AnosAtividadePrincipal = $service->produtividadeMedia3Safras20Anos($atividadePrincipal->produtividade_media_3_safras);
		$produtividade20AnosAtividadeSecundaria = $service->produtividadeMedia3Safras20Anos($atividadeSecundaria->produtividade_media_3_safras);

		// Retorno das atividades
		$retornoAtividadePrincipal = $this->calculaRetornoAtividade($atividadePrincipal, $atividadePrincipal->area_produtiva, $produtividade20AnosAtividadePrincipal);
		$retornoAtividadeSecundaria = $this->calculaRetornoAtividade($atividadeSecundaria, $atividadePrincipal->area_produtiva, $produtividade20AnosAtividadeSecundaria);

		// Resultado Total das Atividades
		$resultadoTotalAtividades = $service->resultadoTotalAtividades(
			$retornoAtividadePrincipal['resultados'],
			$retornoAtividadeSecundaria['resultados']
		);

		// Restauro de APP
		$restauroAPP20Anos = $this->restauroAPP($dadosPropriedade, $estado, $retornoAtividadePrincipal, $retornoAtividadeSecundaria);

		// Restauro de RL
		$restauroRL20Anos = $this->restauroRL($dadosPropriedade, $estado, $retornoAtividadePrincipal, $retornoAtividadeSecundaria);
		
		// Restauro Área Marginal
		$restauroAreaMarginal = $service->restauracaoAreaMarginal($estado->custos_restauros->all(), $dadosPropriedade->area_marginal, $taxasNacionais->inflacao);

		// Restauro Compensacao
		$restauroCompensacao = $service->restauroCompensacao($dadosPropriedade->deficit_rl, $dadosPropriedade->area_marginal, $taxasNacionais->inflacao, $bioma);

		// Custos Financeiros
		$custoFinanceiro = $service->custoFinanceiro($custosFinanceiros, $atividadePrincipal->area_produtiva, $taxasNacionais->inflacao);

		// Reembolso de Linhas de Financiamento
		$custosLinhasFinanciamento = $service->custosLinhasFinanciamento(
			$estado->linhas_financiamento,
			$estado->custos_restauros,
			$dadosPropriedade,
			$atividadePrincipal->area_produtiva,
			$taxasNacionais->juros_selic
		);

		// Calcula o retorno com Capital Próprio
		$retornoRegularizacaoCapitalProprio = $service->retornoRegularizacaoCapitalProprio(
			$resultadoTotalAtividades,
			$restauroAPP20Anos,
			$restauroRL20Anos,
			$restauroCompensacao,
			$restauroAreaMarginal,
			$custoFinanceiro,
			$atividadePrincipal->area_produtiva,
			$taxasNacionais->juros_selic
		);

		// Calcula o retorno com Financiamento
		$retornoRegularizacaoFinanciamento = $service->retornoRegularizacaoFinanciamento(
			$estado->linhas_financiamento,
			$custosLinhasFinanciamento,
			$resultadoTotalAtividades,
			$restauroAPP20Anos,
			$restauroRL20Anos,
			$restauroCompensacao,
			$restauroAreaMarginal,
			$custoFinanceiro,
			$atividadePrincipal->area_produtiva,
			$taxasNacionais->juros_selic,
			$taxasNacionais->inflacao
		);

		// Calcula o retorno sem Regularizacao
		$retornoSemRegularizacao = $service->retornoSemRegularizacao(
			$resultadoTotalAtividades,
			$custoFinanceiro,
			$atividadePrincipal->area_produtiva,
			$taxasNacionais->juros_selic
		);

		// Calcula o custo da regularizacao
		$custoRegularizacao = $service->custoRegularizacao(
			$restauroAPP20Anos,
			$restauroRL20Anos,
			$restauroCompensacao,
			$atividadePrincipal->area_produtiva
		);

		// Calcula o custo de oportunidade
		$custoOportunidade = $service->custoOportunidade(
			$estado->custos_restauros,
			$retornoRegularizacaoCapitalProprio,
			$dadosPropriedade,
			$taxasNacionais,
			$atividadePrincipal->area_produtiva
		);
		
		// Calcula a rentabilidade media apos regularizar
		$rentabilidadeMedia = $service->rentabilidadeMediaAposRegularizar(
			$restauroAPP20Anos,
			$restauroRL20Anos,
			$custoOportunidade,
			$restauroCompensacao['custo_real_negativo'],
			$retornoSemRegularizacao['pgto_hectare'],
			$atividadePrincipal->area_produtiva
		);

		// Calcula a forma de APP e RL mais barata de regularizar
		$formaRegularizarBarata = $service->formaRegularizarBarata(
			$rentabilidadeMedia
		);

		// Calcula a rentabilidade Atual
		$rentabilidadeAtual = $service->rentabilidadeAtual(
			$resultadoTotalAtividades[0],
			$custoFinanceiro['custo_real'][0],
			$atividadePrincipal->area_produtiva
		);

		// Calcula a rentabilidade Atual da Atividade
		$rentabilidadeAtualAtividade = $service->rentabilidadeAtualAtividade(
			$resultadoTotalAtividades[0],
			$custoFinanceiro['custo_real'][0],
			$premissasRegularizacao->atividade,
			$atividadePrincipal,
			$atividadeSecundaria
		);

		return response()->json(
			[
				'retorno_sem_regularizacao' => $retornoSemRegularizacao,
				'retorno_regularizacao_capital_proprio' => $retornoRegularizacaoCapitalProprio,
				'retorno_regularizacao_custo_capital' =>  $custoOportunidade,
				'retorno_regularizacao_financiamento' => $retornoRegularizacaoFinanciamento,
				'custos_regularizacao' => $custoRegularizacao,
				'rentabilidade_regularizaco' => $rentabilidadeMedia,
				'regularizacao_barata' => $formaRegularizarBarata,
				'rentabilidade_atual' => $retornoSemRegularizacao['pgto_hectare'],
				'rentabilidade_atual_atividade' => $rentabilidadeAtualAtividade,
				'linhas_financiamento' => $this->linhasSlugs($estado->linhas_financiamento),
				'restauros' => $this->restauroSlugs($estado->custos_restauros->all())
			],
			200
		);
	}

	/**
	 * Helper - Atualiza valores do uso atual para regularização
	 * @param  [integer] $atividade
	 * @param  [collection] $valores  
	 * @param  [numeric] $mediaArrobasAnimal
	 * @param  [numeric] $animaisVendidos
	 * @return [array]           
	 */
	protected function atualizaValoresAtividade($atividade, $valores, $mediaArrobasAnimal = 0, $animaisVendidos = 0) {
		$service = new CalculadoraService;
		// Calcula a produção atual
		$valores->producao_atual = $service->producaoAtual($atividade, $valores->producao_atual, $mediaArrobasAnimal, $animaisVendidos);
		// Calcula a produtividade média
		$valores->produtividade_media_3_safras = $service->produtividadeMedia($atividade, $valores->produtividade_media_3_safras, $mediaArrobasAnimal);
		// Calcula o preco recebido
		$valores->preco_recebido = $service->precoRecebido($atividade, $valores->preco_recebido, $mediaArrobasAnimal);
		// Calcula a custo de producao
		$valores->custo_producao = $service->custoProducao($atividade, $valores->custo_producao, $mediaArrobasAnimal);
		return $valores->toArray();
	}

	/**
	 * Helper - Calcula Retorno de Atividade por 20 anos
	 * @param [collection] $atividade
	 * @param [array] $produtividade20Anos
	 * @return  [collect]
	 */
	protected function calculaRetornoAtividade($atividade, $areaProdutiva, $produtividade20Anos) {
		$taxasNacionais = TaxasNacionais::first();
		$service = new CalculadoraService;
		// Calcula a receita anual de 20 anos
		$receitaAnual20Anos = $service->receitaAnualAtividade20Anos(
			$atividade->preco_recebido,
			$atividade->area_produtiva,
			$taxasNacionais->inflacao,
			$produtividade20Anos
		);
		// Calcula o custo anual de 20 anos para a atividade
		$custoAnual20Anos = $service->custoAnualAtividade20Anos(
			$atividade->custo_producao,
			$atividade->area_produtiva,
			$taxasNacionais->inflacao,
			$produtividade20Anos
		);
		// Calcula o resultado anual para 20 anos
		$resultadosAnual20Anos = $service->resultadoAnualAtividade20Anos(
			$receitaAnual20Anos,
			$custoAnual20Anos
		);
		// Calcula o resultado anual por hectare para 20 anos
		$resultadosHectareAnual20Anos = $service->resultadosHectareAnualAtividade20Anos(
			$resultadosAnual20Anos,
			$areaProdutiva
		);
		return [
			'receita' => $receitaAnual20Anos,
			'custo' => $custoAnual20Anos,
			'resultados' => $resultadosAnual20Anos,
			'resultadosHectare' => $resultadosHectareAnual20Anos
		];
	}

	/**
	 * Helper - Chama as funcoes para calcular o Restauro de APP
	 * @param  [collection] $dadosPropriedade
	 * @param  [array] $estado
	 * @param  [array] $retornoAtividadePrincipal
	 * @param  [array] $retornoAtividadeSecundaria
	 * @return [array]
	 */
	protected function restauroAPP($dadosPropriedade, $estado, $retornoAtividadePrincipal, $retornoAtividadeSecundaria) {
		$taxasNacionais = TaxasNacionais::first();
		$service = new CalculadoraService;
		// Calcula os custos dos 3 primeiros anos
		$restauroAPP = $service->restauroAPP(
			$dadosPropriedade->deficit_app,
			$estado->custos_restauros->all(),
			$taxasNacionais->inflacao
		);
		// Calcula os custos dos 20 anos
		$restauroAPP = $service->restauroAPP20Anos(
			$retornoAtividadePrincipal,
			$retornoAtividadeSecundaria,
			$taxasNacionais->inflacao,
			$dadosPropriedade->deficit_app,
			$restauroAPP,
			$estado->custos_restauros->all()
		);
		return $restauroAPP;
	}

	/**
	 * Helper - Chama as funcoes para calcular o Restauro de RL
	 * @param  [collection] $dadosPropriedade
	 * @param  [array] $estado
	 * @param  [array] $retornoAtividadePrincipal
	 * @param  [array] $retornoAtividadeSecundaria
	 * @return [array]
	 */
	protected function restauroRL($dadosPropriedade, $estado, $retornoAtividadePrincipal, $retornoAtividadeSecundaria) {
		$taxasNacionais = TaxasNacionais::first();
		$service = new CalculadoraService;
		// Calcula os custos dos 3 primeiros anos
		$restauroRL = $service->restauroRL(
			$dadosPropriedade->deficit_rl,
			$dadosPropriedade->area_marginal,
			$estado->custos_restauros->all(),
			$taxasNacionais->inflacao
		);
		// Calcula os custos dos 20 anos
		$restauroRL = $service->restauroRL20Anos(
			$retornoAtividadePrincipal,
			$retornoAtividadeSecundaria,
			$taxasNacionais->inflacao,
			$dadosPropriedade->deficit_rl,
			$dadosPropriedade->area_marginal,
			$restauroRL,
			$estado->custos_restauros->all()
		);
		return $restauroRL;
	}

	/**
	 * Retorna os Slugs das formas de restauro
	 * @param  array $custosRestauros
	 * @return array                 
	 */
	public function restauroSlugs($custosRestauros) {
		$loopMax = sizeof($custosRestauros);
		$slugs = [];
		for ($index=0; $index < $loopMax; $index++) {
			$slug = str_slug($custosRestauros[$index]->nome);
			$nome = $custosRestauros[$index]->nome;
			array_push($slugs, ['nome' => $nome, 'slug' => $slug]);
		}
		return $slugs;
	}

	/**
	 * Retorna os Slugs das linhas de financiamento
	 * @param  array $linhasFinanciamento
	 * @return array                 
	 */
	public function linhasSlugs($linhasFinanciamento) {
		$loopMax = sizeof($linhasFinanciamento);
		$slugs = [];
		for ($index=0; $index < $loopMax; $index++) {
			$slug = str_slug($linhasFinanciamento[$index]->nome);
			$nome = $linhasFinanciamento[$index]->nome;
			array_push($slugs, ['nome' => $nome, 'slug' => $slug]);
		}
		return $slugs;
	}
}
