<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\InfosLinhasFinanciamento;
use CalculadoraAgroicone\Http\Requests\InfosLinhasFinanciamentoRequest;

class InfosLinhasFinanciamentosController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.infos-linhas-financiamentos.viewInfosLinhasFinanciamentos');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infosLinhasFinanciamentos = InfosLinhasFinanciamento::all();
        return response()->json([
            'infosLinhasFinanciamentos' => $infosLinhasFinanciamentos
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InfosLinhasFinanciamentoRequest $request)
    {
        $infoLinhaFinanciamento = InfosLinhasFinanciamento::create($request->all());
        return response()->json([
            'message' => $infoLinhaFinanciamento->nome . ' criada com sucesso',
            'infoLinhaFinanciamento' => $infoLinhaFinanciamento
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InfosLinhasFinanciamentoRequest $request, $id)
    {
        $infoLinhaFinanciamento = InfosLinhasFinanciamento::find($id);
        $infoLinhaFinanciamento->fill($request->all());
        $infoLinhaFinanciamento->update();
        return response()->json([
            'message' => $infoLinhaFinanciamento->nome . ' atualizada com sucesso',
            'infoLinhaFinanciamento' => $infoLinhaFinanciamento
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        InfosLinhasFinanciamento::find($id)->delete();
        return response()->json([
            'message' => 'Informação excluída com sucesso'
        ]);
    }
}
