<?php

namespace CalculadoraAgroicone\Http\Controllers;

use Illuminate\Http\Request;
use CalculadoraAgroicone\ModelosRestauracoes;
use CalculadoraAgroicone\Http\Requests\ModelosRestauracoesRequest;

class ModelosRestauracoesController extends Controller
{
    /**
     * Display a view
     *
     * @return view
     */
    public function view()
    {
        return view('dashboard.modelos-restauracao.viewModelosRestauracao');
    }

    /**
     * Listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ModelosRestauracao = ModelosRestauracoes::all();
        return response()->json([
            'modelosRestauracao' => $ModelosRestauracao
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModelosRestauracoesRequest $request)
    {
        $modeloRestauracao = ModelosRestauracoes::create($request->all());
        return response()->json([
            'message' => $modeloRestauracao->nome . ' criada com sucesso',
            'modeloRestauracao' => $modeloRestauracao
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ModelosRestauracoesRequest $request, $id)
    {
        $modeloRestauracao = ModelosRestauracoes::find($id);
        $modeloRestauracao->fill($request->all());
        $modeloRestauracao->update();
        return response()->json([
            'message' => $modeloRestauracao->nome . ' atualizada com sucesso',
            'modeloRestauracao' => $modeloRestauracao
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ModelosRestauracoes::find($id)->delete();
        return response()->json([
            'message' => 'Modelo excluído com sucesso'
        ]);
    }
}
