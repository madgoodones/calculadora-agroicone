<?php

namespace CalculadoraAgroicone\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustosPadroesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'estado' => 'required|min:3|max:255',
            'praca' => 'required|min:3|max:255',
            'cultura' => 'required|min:3|max:255',
            'parametro' => 'required|min:3|max:255',
            'valor' => 'required|min:0|numeric',
            'fonte' => 'required|min:3|max:255'
        ];
    }
}
