<?php

namespace CalculadoraAgroicone\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LinhasFinanciamentosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:3|max:255',
            'taxa_juros_aa' => 'required|numeric|min:0|max:99999.99',
            'taxa_basica' => 'required|numeric|min:0|max:99999.99',
            'remuneracao_banco' => 'required|numeric|min:0|max:99999.99',
            'risco_credito' => 'required|numeric|min:0|max:99999.99',
            'prazo' => 'required|integer|min:0',
            'carencia' => 'required|integer|min:0',
            'estado_id' => 'required|integer|min:0'
        ];
    }
}
