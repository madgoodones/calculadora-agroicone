<?php

namespace CalculadoraAgroicone\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustosRestaurosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:3|max:255',
            'ano_1' => 'required|numeric|min:0|max:99999.99',
            'ano_2' => 'required|numeric|min:0|max:99999.99',
            'ano_3' => 'required|numeric|min:0|max:99999.99',
            'estado_id' => 'required|integer|min:0'
        ];
    }
}
