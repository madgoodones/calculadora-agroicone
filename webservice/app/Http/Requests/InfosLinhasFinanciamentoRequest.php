<?php

namespace CalculadoraAgroicone\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InfosLinhasFinanciamentoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|min:3|max:255',
            'objetivos' => 'required|min:6',
            'taxa_juros_final_tomador' => 'required|min:6',
            'prazos_carencias' => 'required|min:6',
            'beneficiarios' => 'required|min:6'
        ];
    }
}
