<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class AtividadesPrincipais extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'nome'
	];
}