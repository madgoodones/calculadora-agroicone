<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class CustosPadroes extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'estado',
		'praca',
		'cultura',
		'parametro',
		'valor',
		'fonte'
	];
}
