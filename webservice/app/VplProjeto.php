<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class VplProjeto extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'crescimento_produtividade_principal',
		'crescimento_produtividade_secundaria'
	];
}
