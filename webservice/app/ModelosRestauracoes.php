<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class ModelosRestauracoes extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'nome',
		'descricao'
	];
}
