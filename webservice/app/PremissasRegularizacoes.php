<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class PremissasRegularizacoes extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array 
	**/
	protected $fillable = [
		'estado',
		'bioma',
		'atividade'
	];
}
