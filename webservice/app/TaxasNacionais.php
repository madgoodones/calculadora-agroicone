<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class TaxasNacionais extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'juros_selic',
		'imposto_renda',
		'taxa_adm',
		'inflacao'
	];
}
