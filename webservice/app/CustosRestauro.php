<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class CustosRestauro extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'nome',
		'ano_1',
		'ano_2',
		'ano_3',
		'estado_id'
	];

	/**
	* Get the Estado that owns the Biomas.
	*/
	public function estado()
	{
		return $this->belongsTo('CalculadoraAgroicone\Estado');
	}
}
