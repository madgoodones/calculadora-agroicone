<?php

namespace CalculadoraAgroicone\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class enviarProjetoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $dados;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dados)
    {
        $this->dados = $dados;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //gerando um nome para o arquivo
        $pathToFile = storage_path('projeto-'.uniqid().'.pdf');
        //salvar a string em uma pasta temporária para servir de anexo
        $base64 = str_replace('data:application/pdf;base64,', '', $this->dados->projeto);
        file_put_contents($pathToFile, base64_decode($base64));
        // Enviar o e-mail
        return $this->subject('[Projeto Agroicone] Resultado do Projeto')
        ->bcc('joel.santos@madgo.com.br', 'Agroicone')
        ->bcc('luciane@agroicone.com.br', 'Luciane')
        ->bcc('juliane@agroicone.com.br', 'Juliane')
        ->attach($pathToFile, [
            'as'=> 'Resultado do Projeto',
            'mime' => 'application/pdf'
        ])
        ->view('mail.enviarProjeto');
    }
}
