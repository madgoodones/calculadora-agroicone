<?php

namespace CalculadoraAgroicone\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class downloadDoProjetoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $dados;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($dados)
    {
        $this->dados = $dados;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[Projeto Agroicone] Resultado do Projeto')
            ->cc('luciane@agroicone.com.br', 'Luciane')
            ->cc('juliane@agroicone.com.br', 'Juliane')
            ->view('mail.downloadDoProjeto');
    }
}
