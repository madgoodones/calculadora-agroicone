<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class DadosPropriedade extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'deficit_rl',
		'deficit_app',
		'area_marginal',
		'app_atual_conservada'
	];
}
