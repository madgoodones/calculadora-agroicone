<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class Nota extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'secao',
		'descricao'
	];
}
