<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'nome', 'uf'
	];

	/**
	 * Get the Linhas de financiamento for Estado
	 */
	public function linhas_financiamento()
	{
		return $this->hasMany('CalculadoraAgroicone\LinhasFinanciamento');
	}
	
	/**
	 * Get the Biomas for Estado
	 */
	public function biomas()
	{
		return $this->hasMany('CalculadoraAgroicone\Bioma');
	}

	/**
	 * Get the Custos de Restauros for Estado
	 */
	public function custos_restauros()
	{
		return $this->hasMany('CalculadoraAgroicone\CustosRestauro');
	}
}
