<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class CustosFinanceiros extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'juros_financiamento',
		'tributo_itr_csr_funrural'
	];
}
