<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class Bioma extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'nome',
		'cenario',
		'estado_id'
	];

	/**
	* Get the Estado that owns the Biomas.
	*/
	public function estado()
	{
		return $this->belongsTo('CalculadoraAgroicone\Estado');
	}
}
