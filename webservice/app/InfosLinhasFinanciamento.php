<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class InfosLinhasFinanciamento extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'nome',
		'objetivos',
		'taxa_juros_final_tomador',
		'prazos_carencias',
		'beneficiarios'
	];
}
