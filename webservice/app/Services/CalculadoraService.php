<?php
namespace CalculadoraAgroicone\Services;

use Finance;

class CalculadoraService {

	/**
	 * RN - Se a atividade for Pecuária o valor é zero e se for Grãos ou Algodão o valor muda
	 * @param  [integer] $atividade
	 * @param  [double] $valor
	 * @return [producao]
	 */
	public function producaoAtual($atividade, $valor, $mediaArrobasAnimal = 0, $animaisVendidos = 0) {
		if ($atividade == 1) return $valor / (1000/15); // Algodão
		elseif($atividade == 3) return $valor / (1000/60); // Grãos
		elseif($atividade == 4) return ($mediaArrobasAnimal * $animaisVendidos) / (1000/15); // Pecuária de Corte
		return $valor; // Cana/Outros
	}

	/**
	 * RN - Se a atividade for Pecuária, Grãos e ou Algodão o valor muda
	 * @param  [integer] $atividade
	 * @param  [double] $valor
	 * @param  [double] $mediaArrobasAnimal
	 * @return [producao]
	 */
	public function produtividadeMedia($atividade, $valor, $mediaArrobasAnimal = 0) {
		if ($atividade == 1) return $valor / (1000/15); // Algodão
		elseif($atividade == 3) return $valor / (1000/60); // Grãos
		elseif($atividade == 4 || $mediaArrobasAnimal != 0) return $valor / (1000/$mediaArrobasAnimal); // Pecuária de Corte
		return $valor; // Cana/Outros
	}

	/**
	 * RN - Se a atividade for Pecuária, Grãos e ou Algodão o valor muda
	 * @param  [integer] $atividade
	 * @param  [double] $valor
	 * @return [producao]
	 */
	public function precoRecebido($atividade, $valor, $mediaArrobasAnimal = 0) {
		if ($atividade == 1) return $valor * (1000/15); // Algodão
		elseif($atividade == 3) return $valor * (1000/60); // Grãos
		elseif($atividade == 4 || $mediaArrobasAnimal != 0) return $valor * (1000/$mediaArrobasAnimal); // Pecuária de Corte
		return $valor; // Cana/Outros
	}

	/**
	 * RN - Se a atividade for Pecuária, Grãos e ou Algodão o valor muda
	 * @param  [integer] $atividade
	 * @param  [double] $valor
	 * @return [producao]
	 */
	public function custoProducao($atividade, $valor, $mediaArrobasAnimal = 0) {
		if ($atividade == 1) return $valor * (1000/15); // Algodão
		elseif($atividade == 3) return $valor * (1000/60); // Grãos
		elseif($atividade == 4 || $mediaArrobasAnimal != 0) return $valor * (1000/$mediaArrobasAnimal); // Pecuária de Corte
		return $valor; // Cana/Outros
	}

	/**
	 * Cálculos - Retorna os valores de produtividade de 20 Anos
	 * @param  [double] $valor
	 * @return [array]
	 */
	public function produtividadeMedia3Safras20Anos($valor, $crescimentoProdutividade = 0.002) {
		$valores = [
			$valor
		];
		for ($index=0; $index <= 19; $index++) { 
			$newValor = $valores[$index] * ( 1 + $crescimentoProdutividade );
			array_push( $valores, $newValor );
		}
		return $valores;
	}

	/**
	 * Cálculos - Retorna os valores de receita anual de 20 anos
	 * @param  [double] $precoRecebido                  
	 * @param  [double] $areaProdutiva                  
	 * @param  [double] $inflacao                       
	 * @param  [array] $produtividadeMedia3Safras20Anos
	 * @return [array]                                 
	 */
	public function receitaAnualAtividade20Anos($precoRecebido, $areaProdutiva, $inflacao, $produtividadeMedia3Safras20Anos) {

		$loopMax = sizeof($produtividadeMedia3Safras20Anos);
		$receitas = [];

		for ($index=0; $index < $loopMax; $index++) { 
			$newValor = $precoRecebido * ( pow( (1 + $inflacao), $index ) ) * $produtividadeMedia3Safras20Anos[$index] * $areaProdutiva;
			array_push( $receitas, $newValor );
		}
		return $receitas;
	}

	/**
	 * Cálculos - Retorna os valores de custo anual de 20 anos            
	 * @param  [double] $areaProdutiva                  
	 * @param  [double] $inflacao                       
	 * @param  [array] $produtividadeMedia3Safras20Anos
	 * @return [array]                                 
	 */
	public function custoAnualAtividade20Anos($custoProducao, $areaProdutiva, $inflacao, $produtividadeMedia3Safras20Anos) {

		$loopMax = sizeof($produtividadeMedia3Safras20Anos);
		$custos = [];

		for ($index=0; $index < $loopMax; $index++) { 
			$newValor = $produtividadeMedia3Safras20Anos[$index] * $custoProducao * ( pow( (1 + $inflacao), $index ) ) * $areaProdutiva;
			array_push( $custos, $newValor );
		}
		return $custos;
	}

	/**
	 * Cálculos - Retorna os valores de resultado anual de 20 anos
	 * @param  [double] $precoRecebido                  
	 * @param  [double] $areaProdutiva                  
	 * @param  [double] $inflacao                       
	 * @param  [array] $produtividadeMedia3Safras20Anos
	 * @return [array]                                 
	 */
	public function resultadoAnualAtividade20Anos($receitas, $custos) {

		$loopMax = sizeof($receitas);
		$resultados = [];

		for ($index=0; $index < $loopMax; $index++) { 
			$newValor = $receitas[$index] - $custos[$index];
			array_push( $resultados, $newValor );
		}
		return $resultados;
	}

	/**
	 * Cálculos - Retorna os valores de resultado anual por hectare de 20 anos
	 * @param  [double] $precoRecebido                  
	 * @param  [double] $areaProdutiva                  
	 * @param  [double] $inflacao                       
	 * @param  [array] $produtividadeMedia3Safras20Anos
	 * @return [array]                                 
	 */
	public function resultadosHectareAnualAtividade20Anos($resultados, $areaProdutiva) {

		$loopMax = sizeof($resultados);
		$resultadosHectare = [];
		// Se a área produtiva for zero não divide e retorna 0
		for ($index=0; $index < $loopMax; $index++) { 
			if ($areaProdutiva==0) $newValor = 0;
			else $newValor = $resultados[$index] / $areaProdutiva;
			array_push( $resultadosHectare, $newValor );
		}
		return $resultadosHectare;
	}

	/**
	 * Cálculos - Calcula o Custo de Restauracao Nominal e Real dos 3 primeiros anos
	 * @param  [double] $deficit_app    
	 * @param  [array] $custosRestauro 
	 * @param  [float] $inflacao       
	 * @return [array]                 
	 */
	public function restauroAPP($deficit_app, $custosRestauro, $inflacao) {
		$loopMax = sizeof($custosRestauro);
		$custos = [];
		// Calcula o Custo de Restauracao Nominal
		for ($index=0; $index < $loopMax; $index++) {
			$ano_1 = $deficit_app * $custosRestauro[$index]->ano_1;
			$ano_2 = $deficit_app * $custosRestauro[$index]->ano_2;
			$ano_3 = $deficit_app * $custosRestauro[$index]->ano_3;
			$slug = str_slug($custosRestauro[$index]->nome);
			array_push($custos,
				[
					'modelo' => [
						'nome' => $custosRestauro[$index]->nome,
						'slug' => $slug
					],
					'custos_restauracao_nominal' => [ 
						$ano_1, 
						$ano_2, 
						$ano_3
					],
					'custos_restauracao_real' => [],
					'perda_receita_real_hectare' => [],
					'custo_total_real' => [],
					'custo_total_real_negativo' => []
					
				]
			);
			// Calcula o Custo de Restauracao Real
			$loopMaxInner = sizeof($custos[$index]['custos_restauracao_nominal']);
			for ($innerIndex=0; $innerIndex < $loopMaxInner; $innerIndex++) {
				$nominal = $custos[$index]['custos_restauracao_nominal'];
				$newValor = $nominal[$innerIndex] * ( pow( ( 1+$inflacao ), $innerIndex ) );
				array_push($custos[$index]['custos_restauracao_real'], $newValor);
			}
		}
		return $custos;
	}

	/**
	 * Cálculos - Calcula os valores de Perda de Receita Real, Custo total e Custo total negativo por 20 anos
	 * @param  [array] $retornoAtividadePrincipal  
	 * @param  [array] $retornoAtividadeSecundaria 
	 * @param  [float] $inflacao                   
	 * @param  [double] $deficit_app                
	 * @param  [array] $restauroAPP                
	 * @param  [array] $custosRestauro             
	 * @return [array]                             
	 */
	public function restauroAPP20Anos($retornoAtividadePrincipal, $retornoAtividadeSecundaria, $inflacao, $deficit_app, $restauroAPP, $custosRestauro) {
		$loopMax = sizeof($custosRestauro);
		// Loop por tipo de Restauro no caso Mudas Nativas, Regeneração Natural Ativa e Semeadura Direta
		for ($index=0; $index < $loopMax; $index++) {
			$loopInnerMax = sizeof($retornoAtividadePrincipal['resultadosHectare']);
			// Incrementa os valores no Array de Restauro de APP os valores de Perda de Receita Real, Custo total e Custo total
			for ($indexInner=0; $indexInner < $loopInnerMax; $indexInner++) {
				$resultadoAtividadePrincipal = $retornoAtividadePrincipal['resultadosHectare'][$indexInner];
				$resultadoAtividadeSecundaria = $retornoAtividadeSecundaria['resultadosHectare'][$indexInner];
				// Perda de Receita Real
				$newValorPerda = ( $deficit_app * ( $resultadoAtividadePrincipal + $resultadoAtividadeSecundaria ) ) * ( pow( ( 1 + $inflacao ), $indexInner ) );
				// Custo total
				if($indexInner < 3) $newValorCustoTotal = $newValorPerda + $restauroAPP[$index]['custos_restauracao_real'][$indexInner];
				else $newValorCustoTotal = $newValorPerda;
				array_push($restauroAPP[$index]['perda_receita_real_hectare'], $newValorPerda);
				array_push($restauroAPP[$index]['custo_total_real'], $newValorCustoTotal);
				array_push($restauroAPP[$index]['custo_total_real_negativo'], -($newValorCustoTotal));
			}
		}
		return $restauroAPP;
	}

	/**
	 * Cálculos - Calcula o Custo de Restauracao Nominal e Real dos 3 primeiros anos
	 * @param  [double] $deficitRL    
	 * @param  [array] $custosRestauro 
	 * @param  [float] $inflacao       
	 * @return [array]                 
	 */
	public function restauroRL($deficitRL, $areaMarginal, $custosRestauro, $inflacao) {
		$loopMax = sizeof($custosRestauro);
		$custos = [];
		// Calcula o Custo de Restauracao Nominal
		for ($index=0; $index < $loopMax; $index++) {
			if ($custosRestauro[$index]->nome != 'compensacao') {
				$ano_1 = ($deficitRL - $areaMarginal) * $custosRestauro[$index]->ano_1;
				$ano_2 = ($deficitRL - $areaMarginal) * $custosRestauro[$index]->ano_2;
				$ano_3 = ($deficitRL - $areaMarginal) * $custosRestauro[$index]->ano_3;
				// echo $deficitRL . ' ' . $areaMarginal;
				// dd([$custosRestauro[$index]->ano_1,$custosRestauro[$index]->ano_2,$custosRestauro[$index]->ano_3]);
				$slug = str_slug($custosRestauro[$index]->nome);
				array_push($custos,
					[
						'modelo' => [
							'nome' => $custosRestauro[$index]->nome,
							'slug' => $slug
						],
						'custos_restauracao_nominal' => [ 
							$ano_1, 
							$ano_2, 
							$ano_3
						],
						'custos_restauracao_real' => [],
						'perda_receita_real_hectare' => [],
						'custo_total_real' => [],
						'custo_total_real_negativo' => []
					]
				);
				// Calcula o Custo de Restauracao Real
				$loopMaxInner = sizeof($custos[$index]['custos_restauracao_nominal']);
				for ($innerIndex=0; $innerIndex < $loopMaxInner; $innerIndex++) {
					$nominal = $custos[$index]['custos_restauracao_nominal'];
					$newValor = $nominal[$innerIndex] * ( pow( ( 1+$inflacao ), $innerIndex ) );
					array_push($custos[$index]['custos_restauracao_real'], $newValor);
				}
			}
		}
		return $custos;
	}

	/**
	 * Cálculos - Calcula os valores de Perda de Receita Real, Custo total e Custo total negativo por 20 anos
	 * @param  [array] $retornoAtividadePrincipal  
	 * @param  [array] $retornoAtividadeSecundaria 
	 * @param  [float] $inflacao                   
	 * @param  [double] $deficitRL                
	 * @param  [array] $restauroRL                
	 * @param  [array] $custosRestauro             
	 * @return [array]                             
	 */
	public function restauroRL20Anos($retornoAtividadePrincipal, $retornoAtividadeSecundaria, $inflacao, $deficitRL, $areaMarginal, $restauroRL, $custosRestauro) {
		$loopMax = sizeof($custosRestauro);
		// Loop por tipo de Restauro no caso Mudas Nativas, Regeneração Natural Ativa e Semeadura Direta
		for ($index=0; $index < $loopMax; $index++) {
			$slug = str_slug($custosRestauro[$index]->nome);
			if ($slug != 'compensacao') {
				$loopInnerMax = sizeof($retornoAtividadePrincipal['resultadosHectare']);
				// Incrementa os valores no Array de Restauro de APP os valores de Perda de Receita Real, Custo total e Custo total
				for ($indexInner=0; $indexInner < $loopInnerMax; $indexInner++) {
					$resultadoAtividadePrincipal = $retornoAtividadePrincipal['resultadosHectare'][$indexInner];
					$resultadoAtividadeSecundaria = $retornoAtividadeSecundaria['resultadosHectare'][$indexInner];
					// Perda de Receita Real
					$newValorPerda = ( ( $resultadoAtividadePrincipal + $resultadoAtividadeSecundaria ) * ( $deficitRL - $areaMarginal ) ) * ( pow( ( 1 + $inflacao ), $indexInner ));
					// Custo total
					if($indexInner < 3) $newValorCustoTotal = $newValorPerda + $restauroRL[$index]['custos_restauracao_real'][$indexInner];
					else $newValorCustoTotal = $newValorPerda;

					array_push($restauroRL[$index]['perda_receita_real_hectare'], $newValorPerda);
					array_push($restauroRL[$index]['custo_total_real'], $newValorCustoTotal);
					array_push($restauroRL[$index]['custo_total_real_negativo'], -($newValorCustoTotal));
				}
			}
		}
		return $restauroRL;
	}

	/**
	 * Cálculos - Calcula a Restauracao de Area Marginal apenas para Regeneração Natural Ativa
	 * @param  [array] $custosRestauro 
	 * @param  [float] $areaMarginal   
	 * @param  [float] $inflacao       
	 * @return [array]                 
	 */
	public function restauracaoAreaMarginal($custosRestauro, $areaMarginal, $inflacao) {
		$loopMax = sizeof($custosRestauro);
		$custos = collect();

		// Calcula o Custo de Restauracao Nominal
		for ($index=0; $index < $loopMax; $index++) {
			$slug = str_slug($custosRestauro[$index]->nome);
			if($slug == 'regeneracao-natural-ativa') {

				$custoAno1 = $areaMarginal * $custosRestauro[$index]->ano_1;
				$custoAno2 = $areaMarginal * $custosRestauro[$index]->ano_2;
				$custoAno3 = $areaMarginal * $custosRestauro[$index]->ano_3;

				$custos->put('custos_restauracao_nominal', [$custoAno1, $custoAno2, $custoAno3]);
				$custoNominal = $custos->first();

				$custosReal = [];
				$custosRealNegativo = [];

				for ($index=0; $index < sizeof($custoNominal); $index++) { 
					array_push($custosReal, $custoNominal[$index] * ( pow( ( 1+$inflacao ), $index ) ) );
					array_push($custosRealNegativo, -($custoNominal[$index] * ( pow( ( 1+$inflacao ), $index ) )) );
				}
				$custos->put('custos_restauracao_real', $custosReal);
				$custos->put('custos_restauracao_real_negativo', $custosRealNegativo);
			}
		}
		return $custos->toArray();
	}

	/**
	 * Cálculos - Calcula os valores de Restauro com Compensação
	 * @param  [type] $deficitRL    [description]
	 * @param  [type] $areaMarginal [description]
	 * @param  [type] $inflacao     [description]
	 * @param  [type] $bioma        [description]
	 * @return [type]               [description]
	 */
	public function restauroCompensacao($deficitRL, $areaMarginal, $inflacao, $bioma) {
		$custoNominal = ( $deficitRL - $areaMarginal ) * $bioma->cenario;
		$custoReal = $custoNominal * ( pow( ( 1+$inflacao ), 0 ) );
		return [
			'custo_nominal' => $custoNominal,
			'custo_real' => $custoReal,
			'custo_real_negativo' => -($custoReal)
		];
	}

	/**
	 * Cálculos - Calcula os custos de Custos Financeiros
	 * @param  [collect] $custosFinanceiros 
	 * @param  [float] $areaProdutiva     
	 * @param  [float] $inflacao          
	 * @return [array]                    
	 */
	public function custoFinanceiro($custosFinanceiros, $areaProdutiva, $inflacao) {
		$custosNominais = [];
		$custosReais = [];
		$custosReaisNegativos = [];
		for ($index=0; $index < 21; $index++) {
			// Pediu para remover dia 24/11/2017 trocar pela linha de baixo
			// $custoNominal = ( $custosFinanceiros->juros_financiamento + $custosFinanceiros->tributo_itr_csr_funrural ) * $areaProdutiva;
			$custoNominal = $custosFinanceiros->juros_financiamento + $custosFinanceiros->tributo_itr_csr_funrural;
			$custoReal = $custoNominal * ( pow( ( 1+$inflacao ), $index ) );
			array_push($custosNominais, $custoNominal);
			array_push($custosReais, $custoReal);
			array_push($custosReaisNegativos, -($custoReal));
		}
		return [
			'custo_nominal' => $custosNominais,
			'custo_real' => $custosReais,
			'custo_real_negativo' => $custosReaisNegativos
		];
	}

	/**
	 * Cálculos - calcula o reembolso das linhas de financiamento
	 * @param  [collect] $linhasFinanciamento
	 * @param  [collect] $custosRestauros    
	 * @param  [float] $deficitRL          
	 * @param  [float] $deficitApp         
	 * @param  [float] $jurosSelic         
	 * @return [array]                     
	 */
	public function custosLinhasFinanciamento($linhasFinanciamento, $custosRestauros, $dadosPropriedade, $areaProdutiva, $jurosSelic) {

		$areaMarginal = $dadosPropriedade->area_marginal;
		$deficitRL = $dadosPropriedade->deficit_rl;
		$deficitApp = $dadosPropriedade->deficit_app;

		$custosRestauros = $custosRestauros->toArray();
		$linhasFinanciamento = $linhasFinanciamento->toArray();

		$custosLinhasFinanciamento=[];

		// Faz a simulação por Linha de Financiamento do Estado escolhido.
		foreach ($linhasFinanciamento as $linha) {
			// Simula para cada Linha seus custos de restauro
			foreach ($custosRestauros as $restauroAPP) {
				$custosAPP = $restauroAPP['ano_1'] + $restauroAPP['ano_2'] + $restauroAPP['ano_3'];
				$slugAPP = str_slug($restauroAPP['nome']);
				// APP não possui compensacao
				if ($slugAPP != 'compensacao') {
					for ($index=0; $index < sizeof($custosRestauros); $index++) { 

						$restauroRL = $custosRestauros[$index];
						$custosRL = $restauroRL['ano_1'] + $restauroRL['ano_2'] + $restauroRL['ano_3'];
						$slugRL = str_slug($restauroRL['nome']);

						// Se for compensação coloca apenas area marginal
						if ($slugRL == 'compensacao') $desembolso = $areaMarginal * $custosRL + $deficitApp * $custosAPP;
						else $desembolso = ($deficitRL+$areaMarginal) * $custosRL + $deficitApp * $custosAPP;
						
						// Calcula Amortizacao, Juros, Saldo Devedor e Reembolso de 20 anos
						$amortizacoes = [];
						$saldosDevedor = [];
						$juros = [];
						$reembolsos = [];
						for ($innerIndex=0; $innerIndex < 21; $innerIndex++) { 
							$prazo = $linha['prazo'];
							$carencia = $linha['carencia'];
							if($innerIndex < $carencia) $amortizacao = 0;
							elseif($innerIndex+1 <= $prazo) $amortizacao = $desembolso / ($prazo-$carencia);
							else $amortizacao = 0;
							if($innerIndex==0) $saldoDevedor = $desembolso;
							else {
								$saldoDevedor = +$saldosDevedor[$innerIndex-1] - $amortizacoes[$innerIndex-1];
								if ($saldoDevedor < 0) {
									$saldoDevedor = 0;
								}
							}

							$juro = +($saldoDevedor * ($linha['taxa_juros_aa']/100));
							$reembolso = +-$amortizacao-$juro;

							array_push($amortizacoes, $amortizacao);
							array_push($saldosDevedor, $saldoDevedor);
							array_push($juros, $juro);
							array_push($reembolsos, $reembolso);
						}
						// Calcula o VPL de Reembolsos
						$vpl = + Finance::npv($jurosSelic, $reembolsos);
						// Valor do Hectare por Ano
						$hectareAno = abs($vpl/($areaProdutiva)) / 21;
						array_push($custosLinhasFinanciamento, [
								'id_linha_financiamento' => $linha['id'],
								'linha_financiamento' => str_slug($linha['nome']),
								'APP' => ['nome' => $restauroAPP['nome'], 'slug' => $slugAPP],
								'RL' => ['nome' => $restauroRL['nome'], 'slug' => $slugRL],
								'desembolso' => $desembolso,
								'reais_hectare_ano' => $hectareAno,
								'amortizacao' => $amortizacoes,
								'saldo_devedor' => $saldosDevedor,
								'juros' => $juros,
								'reembolso' => $reembolsos,
								'vpl' => $vpl
							
						]);
					} // Foreach Custos de Restauro para RL
				}
			} // Foreach Custos de Restauro para APP
		} // Foreach linhas de financiamento
		return $custosLinhasFinanciamento;
	}

	/**
	 * Cálculos - Soma os resultados da Atividade Principal e Secundaria
	 * @param  [array] $resultadoAtividadePrincipal 
	 * @param  [array] $resultadoAtividadeSecundaria
	 * @return [array]                              
	 */
	public function resultadoTotalAtividades($resultadoAtividadePrincipal, $resultadoAtividadeSecundaria) {
		$retornoTotal = [];
		$loopMax = sizeof($resultadoAtividadePrincipal);
		for ($index=0; $index < $loopMax; $index++) { 
			$valor = $resultadoAtividadePrincipal[$index] + $resultadoAtividadeSecundaria[$index];
			array_push($retornoTotal, $valor);
		}
		return $retornoTotal;
	}

	/**
	 * Cálculos - Gera o retorno com capital proprio para todos os tipos de restauro
	 * @param  [array] $resultadoTotalAtividades
	 * @param  [array] $restauroAPP             
	 * @param  [array] $restauroRL              
	 * @param  [array] $restauroAreaMarginal    
	 * @param  [array] $custosFinanceiro        
	 * @param  [float] $areaProdutiva           
	 * @param  [float] $jurosSelic              
	 * @return [array]                          
	 */
	public function retornoRegularizacaoCapitalProprio($resultadoTotalAtividades, $restauroAPP, $restauroRL, $restauroCompensacao, $restauroAreaMarginal, $custosFinanceiro, $areaProdutiva, $jurosSelic) {
		$retornoCapitalProprio = [];
		
		foreach ($restauroAPP as $app) {
			if ($app['modelo']['slug'] != 'compensacao') {
				foreach ($restauroRL as $rl) {
					$custosCapitalProprio = [];
					$retornosAposRegularizacao = [];
					for ($index=0; $index < sizeof($resultadoTotalAtividades); $index++) { 
						$resultadoAtividade = $resultadoTotalAtividades[$index];
						$custoTotalAPP = $app['custo_total_real'][$index];
						if($rl['modelo']['slug'] == 'compensacao') {
							if ($index == 0) $custoTotalRL = $restauroCompensacao['custo_real'];
							else $custoTotalRL = 0;
						}
						else $custoTotalRL = $rl['custo_total_real'][$index];
						$custoFinanceiro = $custosFinanceiro['custo_real'][$index];
						if ($index < 3) $areaMarginal = $restauroAreaMarginal['custos_restauracao_real'][$index];
						else $areaMarginal = 0;
						$valor = $resultadoAtividade - $custoTotalAPP - $custoTotalRL - $areaMarginal - $custoFinanceiro;
						$valorPosRegularizar = $valor / $areaProdutiva;
						array_push($custosCapitalProprio, $valor);
						array_push($retornosAposRegularizacao, $valorPosRegularizar);
					} // Foreach 20 Anos
					// Calcula o VPL do Capital Próprio
					$custosSemOPrimeiroValor = array_slice($custosCapitalProprio,1,20);
					 //$vpl = Finance::npv($jurosSelic, $custosSemOPrimeiroValor) + $custosCapitalProprio[0];
					$vpl = Finance::npv($jurosSelic, $custosCapitalProprio);
					// Calcula o PGTO por Hectare
					$prazo = 21;
					$pgtoHectare = Finance::pmt($jurosSelic, $prazo, -$vpl) / $areaProdutiva;
					array_push($retornoCapitalProprio, [
						'APP' => $app['modelo'],
						'RL' => $rl['modelo'],
						'capital_proprio' => $custosCapitalProprio,
						'vpl' => $vpl,
						'pgto_hectare' => $pgtoHectare,
						'retorno' => $retornosAposRegularizacao
					]);
				} // Foreach Custos de Restauro para RL
			}
		} // Foreach Custos de Restauro para APP
		return $retornoCapitalProprio;
	}

	/**
	 * Calculos - Gera o retorno com financiamento para todos os tipos de restauro e linhas
	 * @param  [array] $linhasFinanciamento      
	 * @param  [array] $custosLinhasFinanciamento
	 * @param  [array] $resultadoTotalAtividades 
	 * @param  [array] $restauroAPP              
	 * @param  [array] $restauroRL               
	 * @param  [array] $restauroCompensacao      
	 * @param  [array] $restauroAreaMarginal     
	 * @param  [array] $custosFinanceiro         
	 * @param  [float] $areaProdutiva            
	 * @param  [float] $jurosSelic               
	 * @param  [float] $inflacao                 
	 * @return [array]                           
	 */
	public function retornoRegularizacaoFinanciamento($linhasFinanciamento, $custosLinhasFinanciamento, $resultadoTotalAtividades, $restauroAPP, $restauroRL, $restauroCompensacao, $restauroAreaMarginal, $custosFinanceiro, $areaProdutiva, $jurosSelic, $inflacao) {
		$retornoFinanciamento = [];
		$retornosAposRegularizacao = [];
		foreach ($linhasFinanciamento as $linha) {
			$slugLinha = str_slug($linha['nome']);
			foreach ($restauroAPP as $app) {
				if ($app['modelo']['slug'] != 'compensacao') {
					foreach ($restauroRL as $rl) {
						$custosFinanciamento = [];
						$custosRegularizacao = [];
						$retornosAposRegularizacao = [];
						for ($index=0; $index < sizeof($resultadoTotalAtividades); $index++) {
							$resultadoAtividade = $resultadoTotalAtividades[$index];
							$perdaReceitaAPP = $app['perda_receita_real_hectare'][$index];
							if ($rl['modelo']['slug'] == 'compensacao') $perdaReceitaRL = 0;
							else $perdaReceitaRL = $rl['perda_receita_real_hectare'][$index];
							$custoFinanceiro = $custosFinanceiro['custo_real'][$index];
							$reembolsoLinha=0;
							$reembolso=0;
							// Busca o valor de reembolso para a APP e RL
							foreach ($custosLinhasFinanciamento as $custosLinha) {
								if ($custosLinha['APP']['slug'] == $app['modelo']['slug'] && $custosLinha['RL']['slug'] == $rl['modelo']['slug'] && $custosLinha['linha_financiamento'] == $slugLinha) {
									$reembolsoLinha = $custosLinha['reembolso'][$index] * ( pow( ( 1+$inflacao/100 ), $index ) );
									$reembolsos = $custosLinha['reembolso'];
								}
							}
							if ($rl['modelo']['slug'] == 'compensacao') {
								if ($index < 1) $custoCompensacao = $restauroCompensacao['custo_real'];
								else $custoCompensacao = 0;
								$perdaReceitaRL=0;
							} else {
								$custoCompensacao=0;
							}
							$custoRegularizacao = (-( -$reembolsoLinha + $perdaReceitaAPP + $perdaReceitaRL) - $custoCompensacao) / 1000;

							$valor = $resultadoAtividade - $custoFinanceiro + $reembolsoLinha - $perdaReceitaAPP -  $custoCompensacao - $perdaReceitaRL;
							$valorPosRegularizar = $valor / $areaProdutiva;
							array_push($custosFinanciamento, $valor);
							array_push($custosRegularizacao, $custoRegularizacao);
							array_push($retornosAposRegularizacao, $valorPosRegularizar);
						} // Foreach 20 Anos
						// Calcula o VPL de Custos com Financiamento
						$vpl = Finance::npv($jurosSelic, $custosFinanciamento);
						// Calcula o PGTO por Hectare
						$prazo = 21;
						$pgtoHectare = Finance::pmt($jurosSelic, $prazo, -$vpl) / $areaProdutiva;
						array_push($retornoFinanciamento, [
							'linha' => [
								'id' => $linha['id'],
								'nome' => $linha['nome'],
								'slug' => $slugLinha
							],
							'APP' => $app['modelo'],
							'RL' => $rl['modelo'],
							'financiamento' => $custosFinanciamento,
							'custos_regularizacao' => $custosRegularizacao,
							'vpl' => $vpl,
							'pgto_hectare' => $pgtoHectare,
							'retorno' => $retornosAposRegularizacao
						]);
					} // Foreach Custos de Restauro para RL
				}
			} // Foreach Custos de Restauro para APP
		}
		return $retornoFinanciamento;
	}

	/**
	 * Calculos - Gera o retorno sem regularizar
	 * @param  [array] $resultadoTotalAtividades
	 * @param  [array] $custosFinanceiro        
	 * @param  [float] $areaProdutiva           
	 * @param  [float] $jurosSelic              
	 * @return [array]                          
	 */
	public function retornoSemRegularizacao($resultadoTotalAtividades, $custosFinanceiro, $areaProdutiva, $jurosSelic) {
		$loopMax = sizeof($resultadoTotalAtividades);
		$semRegularizar = [];
		$retornosAposRegularizacao = [];
		for ($index=0; $index < $loopMax; $index++) { 
			$valor = $resultadoTotalAtividades[$index] - $custosFinanceiro['custo_real'][$index];
			$valorPosRegularizar = $valor / $areaProdutiva;
			array_push($semRegularizar, $valor);
			array_push($retornosAposRegularizacao, $valorPosRegularizar);
		}
		// Calcula o VPL de Custos com Financiamento
		$vpl = Finance::npv($jurosSelic, $semRegularizar);
		// Calcula o PGTO por Hectare
		$prazo = 21;
		$pgtoHectare = Finance::pmt($jurosSelic, $prazo, -$vpl) / $areaProdutiva;
		return [
			'valores' => $semRegularizar,
			'vpl' => $vpl,
			'pgto_hectare' => $pgtoHectare,
			'retorno' => $retornosAposRegularizacao
		];
	}

	/**
	 * Cálculos - Gera o custo da Regularizacao
	 * @param  [array] $restauroAPP         [description]
	 * @param  [array] $restauroRL          [description]
	 * @param  [array] $restauroCompensacao [description]
	 * @param  [float] $areaProdutiva       [description]
	 * @return [array]                      [description]
	 */
	public function custoRegularizacao($restauroAPP, $restauroRL, $restauroCompensacao, $areaProdutiva) {
		$custoRegularizacao = [];
		foreach ($restauroAPP as $APP) {
			if ($APP['modelo']['slug'] != 'compensacao') {
				foreach ($restauroRL as $RL) {
					$custos = [];
					$hectares = [];
					$hectaresNegativo = [];
					for ($index=0; $index < 21; $index++) {
						if ($RL['modelo']['slug'] == 'compensacao') {
							if ($index < 1) $custoRL = $restauroCompensacao['custo_real'];
							else $custoRL = 0;
						} else $custoRL = $RL['custo_total_real'][$index];
						$valor = $APP['custo_total_real'][$index] + $custoRL;
						$hectare = $valor / $areaProdutiva;
						array_push($custos, $valor);
						array_push($hectares, $hectare);
						array_push($hectaresNegativo, -$hectare);
					}
					array_push($custoRegularizacao, [
						'APP' => $APP['modelo'],
						'RL' => $RL['modelo'],
						'custos' => $custos,
						'hectare' => $hectares,
						'hectare_negativo' => $hectaresNegativo
					]);
				}
			}
		}
		return $custoRegularizacao;
	}

	/**
	 * Calculos - Gera o retorno com Capital Proprio considerando o custo de capital
	 * @param  [array] $custosRestauro
	 * @param  [array] $capitalProprio
	 * @param  [array] $propriedade   
	 * @param  [array] $taxas         
	 * @param  [float] $areaProdutiva 
	 * @return [array]                
	 */
	public function custoOportunidade($custosRestauro, $capitalProprio, $propriedade, $taxas, $areaProdutiva) {
		$oportunidades = [];
		$totalCapitalProprio = [];
		$areaMarginal = $propriedade->area_marginal;
		$deficitRL = $propriedade->deficit_rl;
		$deficitAPP = $propriedade->deficit_app;
		$custosRestauro = $custosRestauro->toArray();

		foreach ($custosRestauro as $APP) {
			$slugAPP = str_slug($APP['nome']);
			// Soma os valores de restauro da APP
			$custosAPP = $APP['ano_1'] + $APP['ano_2'] + $APP['ano_3'];
			if ($APP['nome'] != 'compensacao') {
				foreach ($custosRestauro as $RL) {
					$custos = [];
					$retornoTotal = [];
					$slugRL = str_slug($RL['nome']);
					
					foreach ($capitalProprio as $capital) {
						if ($capital['APP']['slug'] == $slugAPP && $capital['RL']['slug'] == $slugRL) {
							$totalCapitalProprio = $capital['capital_proprio'];
						}
					}
					// Soma os valores de restauro da RL
					$custosRL = $RL['ano_1'] + $RL['ano_2'] + $RL['ano_3'];

					// Se for compensação coloca apenas area marginal
					if ($slugRL == 'compensacao') $desembolso =  $areaMarginal * $custosRL + $deficitAPP * $custosAPP;
					else $desembolso = ($deficitRL + $areaMarginal) * $custosRL + $deficitAPP * $custosAPP;


					for ($index=0; $index < 21; $index++) { 
						if ($index == 0) $valor = $desembolso * ( 1 - ( $taxas->taxa_adm + $taxas->imposto_renda  ) );
						else $valor = $custos[$index-1] * ( 1 + $taxas->juros_selic );
						$retorno = $totalCapitalProprio[$index] - $valor;
						array_push($custos, $valor);
						array_push($retornoTotal, $retorno);
					}
					// Calcula o VPL custos e retorno
					$vplCustos = Finance::npv($taxas->juros_selic, $custos);
					$vplRetorno = Finance::npv($taxas->juros_selic, $retornoTotal);
					$prazo = 21;
					// Calcula o PGTO por Hectare
					$pgtoHectareCustos = Finance::pmt($taxas->juros_selic, $prazo, -$vplCustos) / $areaProdutiva;
					$pgtoHectareRetorno = Finance::pmt($taxas->juros_selic, $prazo, -$vplRetorno) / $areaProdutiva;
					array_push($oportunidades, [
						'APP' => ['nome' => $APP['nome'], 'slug' => $slugAPP],
						'RL' => ['nome' => $RL['nome'], 'slug' => $slugRL],
						'custos' => $custos,
						'vpl_custos' => $vplCustos,
						'pgto_hectare_custos' => $pgtoHectareCustos,
						'retorno' => $retornoTotal,
						'vpl_retorno' => $vplRetorno,
						'pgto_hectare_retorno' => $pgtoHectareRetorno
					]);
				}
			}
		}
		return $oportunidades;
	}

	/**
	 * Calculos - Gera a rentabilidade média para as formas de restauro
	 * @param  [array] $restauroAPP          [description]
	 * @param  [array] $restauroRL           [description]
	 * @param  [array] $custoOportunidades   [description]
	 * @param  [double] $compensacao          [description]
	 * @param  [double] $pgtoSemRegularizacao [description]
	 * @param  [float] $areaProducao         [description]
	 * @return [array]                       [description]
	 */
	public function rentabilidadeMediaAposRegularizar($restauroAPP, $restauroRL, $custoOportunidades, $compensacao, $pgtoSemRegularizacao, $areaProducao) {
		$rentabilidadeMediaAposRegularizar = [];
		foreach($restauroAPP as $APP) {
			if ($APP['modelo']['slug'] != 'compensacao' ) {
				$slugAPP = $APP['modelo']['slug'];
				$custosTotalNegativoAPP = $APP['custo_total_real_negativo'];

				$loopMax = sizeof($custosTotalNegativoAPP);
				foreach($restauroRL as $RL) {
					$slugRL = $RL['modelo']['slug'];
					$custosTotalRL = $RL['custo_total_real_negativo'];
					$rentabilidades = [];
					for ($index=0; $index < $loopMax; $index++) { 
						if ($slugRL == 'compensacao') {
							if ($index==0) $valor = $custosTotalNegativoAPP[$index] + $compensacao;
							else $valor = $custosTotalNegativoAPP[$index] + 0;
						}
						else $valor = $custosTotalNegativoAPP[$index] + $custosTotalRL[$index];
						array_push($rentabilidades, $valor);
					}
					$mediaRentabilidades = collect($rentabilidades)->avg();
					$pgtoCustoOportunidade=0;
					foreach($custoOportunidades as $oportunidade) {
						if ($oportunidade['APP'] == $slugAPP && $oportunidade['RL'] == $slugRL) {
							$pgtoCustoOportunidade = $oportunidade['pgto_hectare_custos'];
						}
					}
					if ($slugRL == 'compensacao') $rentabilidadeMedia = $pgtoSemRegularizacao + $mediaRentabilidades / $areaProducao;
					else $rentabilidadeMedia = $pgtoSemRegularizacao + $mediaRentabilidades / $areaProducao - $pgtoCustoOportunidade;

					array_push($rentabilidadeMediaAposRegularizar,
						[
							'APP' => $APP['modelo'],
							'RL' => $RL['modelo'],
							'rentabilidades' => $rentabilidades,
							'rentabilidade_media' => $rentabilidadeMedia
						]
					);
				}
			}
		}
		return $rentabilidadeMediaAposRegularizar;
	}

	/**
	 * Calculo - Gera a forma de regularizar mais barata
	 * @param  array $rentabilidadeMediaAposRegularizar
	 * @return array
	 */
	public function formaRegularizarBarata($rentabilidadeMediaAposRegularizar) {
		$max = collect($rentabilidadeMediaAposRegularizar)->max('rentabilidade_media');
		foreach ($rentabilidadeMediaAposRegularizar as $rentabilidade) {
			if ($rentabilidade['rentabilidade_media'] === $max) {
				return $rentabilidade;
			}
		}
		return 'Não encontrou o mais barato.';
	}

	/**
	 * Calcula a rentabilidade atual do produtor
	 * @param  double $retornoTotalAtividades
	 * @param  double $custoFinanceiro
	 * @param  double $areaProdutiva 
	 * @return double
	 */
	public function rentabilidadeAtual($retornoTotalAtividades, $custoFinanceiro, $areaProdutiva) {
		return ( $retornoTotalAtividades - $custoFinanceiro ) / $areaProdutiva;
	}

	/**
	 * Calcula a rentabilidade atual conforme a atividade selecionada
	 * @param  double $retornoTotalAtividades
	 * @param  double $custoFinanceiro       
	 * @param  integer $atividade             
	 * @param  array $atividadePrincipal    
	 * @param  array $atividadeSecundaria   
	 * @return double                        
	 */
	public function rentabilidadeAtualAtividade($retornoTotalAtividades, $custoFinanceiro, $atividade, $atividadePrincipal, $atividadeSecundaria) {
		$valorPrincipal = ( $retornoTotalAtividades - $custoFinanceiro );
		$valorAtividadePrincipal = 0;
		if ($atividade == 1) $valorAtividadePrincipal = (1000/15); // Algodao
		else if ($atividade == 2) $valorAtividadePrincipal = 1; // Cana
		else if ($atividade == 3) $valorAtividadePrincipal = (1000/60); // Grãos
		else if ($atividade == 4) $valorAtividadePrincipal = $atividadePrincipal->animais_vendidos; // Grãos

		$valorPrincipal = $valorPrincipal / ( $atividadePrincipal->producao_atual * $valorAtividadePrincipal );
		$valorSecundaria = 0;
		if ($atividadeSecundaria->cultura != 0) {
			if ($atividadeSecundaria->cultura == 1) $valorSecundaria = $atividadeSecundaria->producao_atual * (1000/15) * 4;
			else if($atividadeSecundaria->cultura == 3) $valorSecundaria = $atividadeSecundaria->producao_atual * (1000/60);
			else $valorSecundaria = 0;
		}
		return $valorPrincipal + $valorSecundaria;
	}
}