<?php

namespace CalculadoraAgroicone;

use Illuminate\Database\Eloquent\Model;

class LinhasFinanciamento extends Model
{
	/**
	* The attributes that are mass assignable.
	*
	* @var array
	*/
	protected $fillable = [
		'nome',
		'taxa_juros_aa',
		'taxa_basica',
		'remuneracao_banco',
		'risco_credito',
		'prazo',
		'carencia',
		'estado_id'
	];

	/**
	* Get the Estado that owns the linhas de financimento.
	*/
	public function estado()
	{
		return $this->belongsTo('CalculadoraAgroicone\Estado');
	}
}
