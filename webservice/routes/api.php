<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' =>[ 'api']], function () {

	Route::resource('users', 'UsersController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('estados', 'EstadosController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('linhas-financiamentos', 'LinhasFinanciamentosController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('biomas', 'BiomasController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('custos-restauros', 'CustosRestaurosController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('taxas-nacionais', 'TaxasNacionaisController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('vpl-projetos', 'VplProjetoController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('modelos-restauracao', 'ModelosRestauracoesController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('custos-padrao', 'CustosPadroesController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('infos-linhas-financiamentos', 'InfosLinhasFinanciamentosController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('atividades-principais', 'AtividadesPrincipaisController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::resource('notas', 'NotasController', ['only' => ['index', 'store', 'update', 'destroy']]);

	Route::post('auth-app', 'Auth\LoginController@authApi');

	Route::post('calculate', 'CalculadoraController@calculate');

	Route::post('downloadDoProjeto', 'downloadDoProjetoController@downloadDoProjeto');

});