<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require_once "auth/routes.php"; // Auth routes
require_once "dashboard/routes.php"; // Dashboard routes

Route::get('/', 'Pages\AdminController@view')->name('admin')->middleware('auth');

Route::get('/cache', function() {
  $exitCode = Artisan::call('cache:clear');
  return redirect('');
});
Route::get('/view-cache', function() {
  $exitCode = Artisan::call('view:clear');
  return redirect('');
});
Route::get('/config-cache', function() {
  $exitCode = Artisan::call('config:cache');
});