<?php
Route::get('users', 'UsersController@view')->name('users.view')->middleware('auth');

Route::get('estados', 'EstadosController@view')->name('estados.view')->middleware('auth');

Route::get('linhas-financiamentos', 'LinhasFinanciamentosController@view')->name('linhasFinanciamentos.view')->middleware('auth');

Route::get('biomas', 'BiomasController@view')->name('biomas.view')->middleware('auth');

Route::get('custos-restauros', 'CustosRestaurosController@view')->name('custosRestauros.view')->middleware('auth');

Route::get('taxas-nacionais', 'TaxasNacionaisController@view')->name('taxasNacionais.view')->middleware('auth');

Route::get('vpl-projetos', 'VplProjetoController@view')->name('vplProjeto.view')->middleware('auth');

Route::get('modelos-restauracao', 'ModelosRestauracoesController@view')->name('modelosRestauracao.view')->middleware('auth');

Route::get('custos-padrao', 'CustosPadroesController@view')->name('custosPadrao.view')->middleware('auth');

Route::get('infos-linhas-financiamentos', 'InfosLinhasFinanciamentosController@view')->name('infosLinhasFinanciamentos.view')->middleware('auth');

Route::get('atividades-principais', 'AtividadesPrincipaisController@view')->name('atividadesPrincipais.view')->middleware('auth');

Route::get('notas', 'NotasController@view')->name('notas.view')->middleware('auth');