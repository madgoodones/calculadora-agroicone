<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxasNacionaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('taxas_nacionais', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('juros_selic', 7, 5);
            $table->decimal('imposto_renda', 7, 5);
            $table->decimal('taxa_adm', 7, 5);
            $table->decimal('inflacao', 7, 5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('taxas_nacionais');
    }
}
