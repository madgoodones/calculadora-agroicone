<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustosPadroesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('custos_padroes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('estado', 255);
            $table->text('praca')->nullable();
            $table->text('cultura');
            $table->text('parametro');
            $table->string('valor', 255);
            $table->text('fonte');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custos_padroes');
    }
}
