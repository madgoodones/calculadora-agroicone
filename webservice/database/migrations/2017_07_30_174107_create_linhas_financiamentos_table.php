<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinhasFinanciamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('linhas_financiamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estado_id')->unsigned();
            $table->string('nome', 255);
            $table->decimal('taxa_juros_aa', 7, 2);
            $table->decimal('taxa_basica', 7, 2);
            $table->decimal('remuneracao_banco', 7, 2);
            $table->decimal('risco_credito', 7, 2);
            $table->integer('prazo');
            $table->integer('carencia');
            $table->foreign('estado_id')
            ->references('id')
            ->on('estados')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('linhas_financiamentos');
    }
}
