<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVplProjetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('vpl_projetos', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('crescimento_produtividade_principal', 7, 3);
            $table->decimal('crescimento_produtividade_secundaria', 7, 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vpl_projetos');
    }
}
