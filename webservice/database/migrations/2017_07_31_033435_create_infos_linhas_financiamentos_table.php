<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfosLinhasFinanciamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create('infos_linhas_financiamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome', 255);
            $table->text('objetivos');
            $table->text('taxa_juros_final_tomador');
            $table->text('prazos_carencias');
            $table->text('beneficiarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('infos_linhas_financiamentos');
    }
}
