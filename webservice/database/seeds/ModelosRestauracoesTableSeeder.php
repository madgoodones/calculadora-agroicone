<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ModelosRestauracoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modelos_restauracoes')->insert(
			[
				[
					'nome' => 'Regeneração Natural Passiva',
					'descricao' => 'Retorno espontâneo da vegetação sem a intervenção humana (custo zero)'
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'descricao' => 'Conjunto de intervenções para condução da vegetação ao seu estágio original'
				],
				[
					'nome' => 'Semeadura Direta',
					'descricao' => 'Plantio das sementes florestais com maquinário agrícola'
				]
			]
		);
    }
}
