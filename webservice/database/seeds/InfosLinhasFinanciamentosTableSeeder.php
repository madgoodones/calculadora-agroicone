<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InfosLinhasFinanciamentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('infos_linhas_financiamentos')->insert(
			[
				[
					'nome' => 'Pronaf Floresta',
					'objetivos' => 'Apoio a projetos técnicos referentes à recomposição e manutenção de APPs e RL e recuperação de áreas degrada-das, para o cumprimento de legislação ambiental.',
					'taxa_juros_final_tomador' => '2,5% a.a.',
					'prazos_carencias' => 'Até 20 anos, incluída a carência de até 12 anos para projetos de sistemas agroflorestais.12 Até 12 anos, com carência de até 8 anos nos demais casos.',
					'beneficiarios' => 'Agricultores e produtores rurais que compõem as unidades fami-liares de produção rural.'
				],
				[
					'nome' => 'Pronaf Eco',
					'objetivos' => 'Implantar, utilizar e/ou recuperar proje-tos de silvicultura (ato de implantar ou manter povoamentos florestais gerado-res de diferentes produtos, madeireiros e não madeireiros).',
					'taxa_juros_final_tomador' => 'Varia de 2,5% a 5,5% a.a., de acordo com o valor da operação.',
					'prazos_carencias' => 'Até 12 anos, incluídos até 8 anos de carência',
					'beneficiarios' => 'Agricultores e produtores rurais que compõem as unidades fami-liares de produção rural.'
				],
				[
					'nome' => 'BNDES Meio Ambiente',
					'objetivos' => 'Apoio a investimentos envolvendo recu-peração e conservação de ecossistemas e biodiversidade, sistemas de gestão e recuperação de passivos ambientais, podendo ser financiados projetos para recuperação de áreas de RL/ APP degra-dadas ou utilizadas para outros fins.',
					'taxa_juros_final_tomador' => 'Apoio direto do BNDES: varia de 9,0% a 11,68% a.a. Apoio indireto do BNDES: varia de 9,1% até 9,5% a.a.+ taxa de remuneração da instituição financeira credenciada.',
					'prazos_carencias' => 'Até 15 anos e 8 de carência',
					'beneficiarios' => 'Sociedades com sede e admi-nistração no País, de controle nacional ou estrangeiro; empre-sários individuais; associações e fundações; pessoas jurídicas de direito público.'
				],
				[
					'nome' => 'ABC Florestal',
					'objetivos' => 'Implantação, manutenção e melhoramen-to do manejo de florestas comerciais, inclusive aquelas destinadas ao uso in-dustrial ou à produção de carvão vegetal.',
					'taxa_juros_final_tomador' => 'Varia entre 7,5% a 8,0 % a.a.',
					'prazos_carencias' => 'Até 12 anos, com 36 meses de ca-rência para projetos de sistemas produtivos de Integração Lavoura--Pecuária-Floresta (ILPF). Até 12 anos, com 8 anos de ca-rência para projetos de florestas comerciais.',
					'beneficiarios' => 'Produtores rurais e suas coope-rativas, inclusive para repasse a associados.'
				],
				[
					'nome' => 'ABC Ambiental',
					'objetivos' => 'Adequação ou regularização das proprie-dades rurais frente à legislação ambiental, inclusive recuperação de RL/APP.',
					'taxa_juros_final_tomador' => 'Varia entre 7,5% a 8,0 % a.a.',
					'prazos_carencias' => 'Até 15 anos, com carência de até 12 meses.',
					'beneficiarios' => 'Produtores rurais e suas coope-rativas, inclusive para repasse a associados.'
				],
				[
					'nome' => 'FNE Verde',
					'objetivos' => 'Promover o desenvolvimento de em-preendimentos e atividades econômicas que propiciem a preservação, a conser-vação, o controle e/ou a recuperação do meio ambiente, com foco na sustentabi-lidade e competitividade das empresas e cadeias produtivas, incluindo investimen-tos em recuperação e regularização de APPs e RL.',
					'taxa_juros_final_tomador' => '8,53% a.a. (ou 7,2505% a.a. quando aplicado bônus de adimplência)',
					'prazos_carencias' => 'Até 20 anos, incluída carência de até 12 anos.',
					'beneficiarios' => 'Produtores rurais, pessoas físicas ou jurídicas; empresas industriais, agroindustriais, comerciais e de prestação de serviços; cooperati-vas de produtores rurais e asso-ciações.'
				],
				[
					'nome' => 'FNO Biodiversidade',
					'objetivos' => 'Destinada a empreendimentos voltados para a regularização e recuperação de áreas de RL e APPs degradadas/alteradas das propriedades rurais.',
					'taxa_juros_final_tomador' => 'Varia de 7,65% até 10% a.a., de acordo com a variação da receita bruta.',
					'prazos_carencias' => 'Investimento fixo ou misto: até 12 anos, incluída a carência, poden-do, para culturas de longo ciclo de maturação, ser estendido até 20 anos, incluída a carência de até 12 anos; Semifixo: até 10 anos, incluída a carência de até 6 anos; e custeio e/ou comercialização até 2 anos.',
					'beneficiarios' => 'Pessoas físicas que se caracte-rizem como produtores rurais; populações tradicionais da Amazônia não contempladas pelo Pronaf; pessoas jurídicas de direito privado, inclusive empre-sas individuais, associações e cooperativas.'
				],
				[
					'nome' => 'FCO Verde',
					'objetivos' => 'Financiar investimentos, custeio agrícola,   custeio associado a projeto de investimento, e serviços e custos relacionados à regularização ambiental e fundiária dos imóveis rurais e à implantação de siste-mas produtivos e tecnologias voltadas    à mitigação das emissões de gases de   efeito estufa.',
					'taxa_juros_final_tomador' => '8,53% a.a. (ou 7,2505% a.a. quando aplicado bônus de adimplência).',
					'prazos_carencias' => 'Até 20 anos, incluído o período de carência de até 12 anos.',
					'beneficiarios' => 'Produtores rurais, na condição de  pessoas físicas e jurídicas, suas cooperativas de produção e asso-ciações de produtores, desde que se dediquem à atividade produtiva   no setor rural.'
				]
			]
		);
    }
}
