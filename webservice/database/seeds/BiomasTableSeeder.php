<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class BiomasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('biomas')->insert(
			[
				// BAHIA
				[
					'nome' => 'Cerrado',
					'cenario' => 1678.15,
					'estado_id' => 1
				],
				[
					'nome' => 'Mata Atlântica',
					'cenario' => 6261.02,
					'estado_id' => 1
				],
				// MARANHÃO
				[
					'nome' => 'Amazônia',
					'cenario' =>  1238.95,
					'estado_id' => 2
				],
				[
					'nome' => 'Cerrado',
					'cenario' => 303.09,
					'estado_id' => 2
				],
				// MATO GROSSO
				[
					'nome' => 'Amazônia',
					'cenario' =>  4498.08,
					'estado_id' => 3
				],
				[
					'nome' => 'Cerrado',
					'cenario' => 2583.84,
					'estado_id' => 3
				],
				// MATO GROSSO DO SULL
				[
					'nome' => 'Cerrado',
					'cenario' => 4038.17,
					'estado_id' => 4
				],
				[
					'nome' => 'Mata Atlântica',
					'cenario' =>  16427.08,
					'estado_id' => 4
				],
				// MINAS GERAIS
				[
					'nome' => 'Cerrado',
					'cenario' => 740.95,
					'estado_id' => 5
				],
				[
					'nome' => 'Mata Atlântica',
					'cenario' =>  11236.50,
					'estado_id' => 5
				],
				// PARÁ
				[
					'nome' => 'Amazônia',
					'cenario' => 992.98,
					'estado_id' => 6
				],
				// PARANÁ
				[
					'nome' => 'Cerrado',
					'cenario' => 8907.37,
					'estado_id' => 7
				],
				[
					'nome' => 'Mata Atlântica',
					'cenario' =>  22670.32,
					'estado_id' => 7
				],
				// PIAUÍ
				[
					'nome' => 'Cerrado',
					'cenario' => 524.87,
					'estado_id' => 8
				],
				// SÃO PAULO
				[
					'nome' => 'Cerrado',
					'cenario' => 28670.01,
					'estado_id' => 9
				],
				[
					'nome' => 'Mata Atlântica',
					'cenario' =>  7858.45,
					'estado_id' => 9
				],
				// TOCANTIS
				[
					'nome' => 'Amazônia',
					'cenario' =>  3913.85,
					'estado_id' => 10
				],
				[
					'nome' => 'Cerrado',
					'cenario' => 843.40,
					'estado_id' => 10
				]
			]
		);
    }
}
