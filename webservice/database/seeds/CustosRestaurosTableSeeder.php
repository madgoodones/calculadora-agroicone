<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CustosRestaurosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('custos_restauros')->insert(
			[
				// BAHIA
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 6569.26,
					'ano_2' => 835.41,
					'ano_3' => 835.41,
					'estado_id' => 1
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 1
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 2842.30,
					'ano_2' => 229.80,
					'ano_3' => 229.80,
					'estado_id' => 1
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 1
				],
				// MARANHÃO
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 6569.26,
					'ano_2' => 835.41,
					'ano_3' => 835.41,
					'estado_id' => 2
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 2
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 2842.30,
					'ano_2' => 229.80,
					'ano_3' => 229.80,
					'estado_id' => 2
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 2
				],
				// MATO GROSSO
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 8098.45,
					'ano_2' => 816.24,
					'ano_3' => 816.24,
					'estado_id' => 3
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 386.86,
					'ano_2' => 193.43,
					'ano_3' => 193.43,
					'estado_id' => 3
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 1896.86,
					'ano_2' => 222.42,
					'ano_3' => 222.42,
					'estado_id' => 3
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 386.86,
					'ano_2' => 193.43,
					'ano_3' => 193.43,
					'estado_id' => 3
				],
				// MATO GROSSO DO SUL
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 8239.27,
					'ano_2' => 821.41,
					'ano_3' => 821.41,
					'estado_id' => 4
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 387.19,
					'ano_2' => 189.10,
					'ano_3' => 189.10,
					'estado_id' => 4
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 2828.67,
					'ano_2' => 226.20,
					'ano_3' => 226.20,
					'estado_id' => 4
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 387.19,
					'ano_2' => 189.10,
					'ano_3' => 189.10,
					'estado_id' => 4
				],
				// MINAS GERAIS
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 6372.98,
					'ano_2' => 1100.34,
					'ano_3' => 1100.34,
					'estado_id' => 5
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 399.97,
					'ano_2' => 199.99,
					'ano_3' => 199.99,
					'estado_id' => 5
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 2973.10,
					'ano_2' => 305.76,
					'ano_3' => 305.76,
					'estado_id' => 5
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 399.97,
					'ano_2' => 199.99,
					'ano_3' => 199.99,
					'estado_id' => 5
				],
				// PARÁ
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 6569.26,
					'ano_2' => 835.41,
					'ano_3' => 835.41,
					'estado_id' => 6
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 6
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 2842.30,
					'ano_2' => 229.80,
					'ano_3' => 229.80,
					'estado_id' => 6
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 6
				],
				// PARANÁ
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 6372.98,
					'ano_2' => 1100.34,
					'ano_3' => 1100.34,
					'estado_id' => 7
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 399.97,
					'ano_2' => 199.99,
					'ano_3' => 199.99,
					'estado_id' => 7
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 2973.10,
					'ano_2' => 305.76,
					'ano_3' => 305.76,
					'estado_id' => 7
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 399.97,
					'ano_2' => 199.99,
					'ano_3' => 199.99,
					'estado_id' => 7
				],
				// PIAUÍ
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 6569.26,
					'ano_2' => 835.41,
					'ano_3' => 835.41,
					'estado_id' => 8
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 8
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 2842.30,
					'ano_2' => 229.80,
					'ano_3' => 229.80,
					'estado_id' => 8
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 8
				],
				// SÃO PAULO
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 6372.98,
					'ano_2' => 1100.34,
					'ano_3' => 1100.34,
					'estado_id' => 9
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 399.97,
					'ano_2' => 199.99,
					'ano_3' => 199.99,
					'estado_id' => 9
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 2973.10,
					'ano_2' => 305.76,
					'ano_3' => 305.76,
					'estado_id' => 9
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 399.97,
					'ano_2' => 199.99,
					'ano_3' => 199.99,
					'estado_id' => 9
				],
				// TOCANTINS
				[
					'nome' => 'Mudas Nativas',
					'ano_1' => 6569.26,
					'ano_2' => 835.41,
					'ano_3' => 835.41,
					'estado_id' => 10
				],
				[
					'nome' => 'Regeneração Natural Ativa',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 10
				],
				[
					'nome' => 'Semeadura Direta',
					'ano_1' => 2842.30,
					'ano_2' => 229.80,
					'ano_3' => 229.80,
					'estado_id' => 10
				],
				[
					'nome' => 'Compensação',
					'ano_1' => 386.17,
					'ano_2' => 193.09,
					'ano_3' => 193.09,
					'estado_id' => 10
				],
			]
		);
    }
}
