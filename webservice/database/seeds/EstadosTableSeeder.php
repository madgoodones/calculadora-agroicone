<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class EstadosTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('estados')->insert(
			[
				['nome' => 'Bahia', 'uf' => 'BA'],
				['nome' => 'Maranhão', 'uf' => 'MA'],
				['nome' => 'Mato Grosso', 'uf' => 'MT'],
				['nome' => 'Mato Grosso do Sul', 'uf' => 'MS'],
				['nome' => 'Minas Gerais', 'uf' => 'MG'],
				['nome' => 'Pará', 'uf' => 'PA'],
				['nome' => 'Paraná', 'uf' => 'PR'],
				['nome' => 'Piauí', 'uf' => 'PI'],
				['nome' => 'São Paulo', 'uf' => 'SP'],
				['nome' => 'Tocantins', 'uf' => 'TO']
			]
		);
	}
}
