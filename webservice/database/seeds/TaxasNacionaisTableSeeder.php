<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TaxasNacionaisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('taxas_nacionais')->insert(
			[
				'juros_selic' => 0.1288,
	            'imposto_renda' => 0.15,
	            'taxa_adm' => 0.01,
	            'inflacao' => 0.04358
			]
		);
    }
}
