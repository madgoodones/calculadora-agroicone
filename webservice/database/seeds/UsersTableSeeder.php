<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Administrator',
                'email' => 'joel.santos@madgo.com.br',
                'password' => Hash::make('123')
            ],
            [
                'name' => 'Agroicone',
                'email' => 'admin@agroicone.com.br',
                'password' => Hash::make('123')
            ],
        ]);
    }
}
