<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CustosPadroesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('custos_padroes')->insert(
			[
				// SOJA
				[
					'estado' => 'Bahia',
					'praca' => 'Barreiras : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '37,5',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Goiás',
					'praca' => 'Rio Verde : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '35,12',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Goiás',
					'praca' => 'Cristalina : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '37,43',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Maranhão',
					'praca' => 'Balsas : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '50',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Mato Grosso',
					'praca' => 'Primavera do Leste : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '56,06',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Mato Grosso',
					'praca' => 'Campo Novo Parecis : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '47,51',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Mato Grosso',
					'praca' => 'Sorriso : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '47,44',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Mato Grosso do Sul',
					'praca' => 'Chapadão do Sul : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '50,65',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Minas Gerais',
					'praca' => 'Unaí : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '44,58',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Paraná',
					'praca' => 'Campo Mourão : Soja',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '31,82',
					'fonte' => 'Conab'
				],
				// MILHO
				[
					'estado' => 'Bahia',
					'praca' => 'Barreiras : Milho',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '22,67',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Goiás',
					'praca' => 'Rio Verde : Milho',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '22,56',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Maranhão',
					'praca' => 'Balsas : Milho',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '22,51',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Minas Gerais',
					'praca' => 'Unaí : Milho',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '23,19',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Mato Grosso do Sul',
					'praca' => 'Chapadão do Sul : Milho',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '19,01',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Paraná',
					'praca' => 'Campo Mourão : Milho',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '19,64',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Paraná',
					'praca' => 'Londrina : Milho',
					'cultura' => 3,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '20,88',
					'fonte' => 'Conab'
				],
				// Algodao
				[
					'estado' => 'Paraná',
					'praca' => 'Barreiras : Algodão',
					'cultura' => 1,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '20,88',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Mato Grosso do Sul',
					'praca' => 'Chapadão do Sul : Algodão',
					'cultura' => 1,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '20,88',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Mato Grosso',
					'praca' => 'Campo Novo Parecis : Algodão',
					'cultura' => 1,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '20,88',
					'fonte' => 'Conab'
				],
				[
					'estado' => 'Mato Grosso',
					'praca' => 'Sorriso : Algodão',
					'cultura' => 1,
					'parametro' => 'Custo da Produção (R$/sc (60Kg))',
					'valor' => '20,88',
					'fonte' => 'Conab'
				]
			]
		);
    }
}
