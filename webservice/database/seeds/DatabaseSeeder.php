<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(EstadosTableSeeder::class);
        $this->call(LinhasFinanciamentoTableSeeder::class);
        $this->call(BiomasTableSeeder::class);
        $this->call(CustosRestaurosTableSeeder::class);
        $this->call(TaxasNacionaisTableSeeder::class);
        $this->call(VplProjetosTableSeeder::class);
        $this->call(ModelosRestauracoesTableSeeder::class);
        $this->call(CustosPadroesTableSeeder::class);
        $this->call(InfosLinhasFinanciamentosTableSeeder::class);
        $this->call(AtividadesPrincipaisTableSeeder::class);
        $this->call(NotasTableSeeder::class);
    }
}
