<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AtividadesPrincipaisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('atividades_principais')->insert(
			[
				['nome' => 'Algodão (@ 15 Kg)'],
				['nome' => 'Cana (ton)'],
				['nome' => 'Grãos (sc 60 Kg)'],				
				['nome' => 'Pecuária de Corte']
			]
		);
    }
}
