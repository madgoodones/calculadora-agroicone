<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class LinhasFinanciamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('linhas_financiamentos')->insert(
			[
				// BAHIA
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 1
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 1
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 1
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 1
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 1
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 1
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 1
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 1
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 1
				],
				[
					'nome' => 'FNE Verde 8.53',
					'taxa_juros_aa' => 8.53,
					'taxa_basica' => 8.53,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 1
				],
				[
					'nome' => 'FNE Verde 7.25',
					'taxa_juros_aa' => 7.25,
					'taxa_basica' => 7.25,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 1
				],
				// MARANHÃO
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 2
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 2
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 2
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 2
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 2
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 2
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 2
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 2
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 2
				],
				[
					'nome' => 'FNE Verde 8.53',
					'taxa_juros_aa' => 8.53,
					'taxa_basica' => 8.53,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 2
				],
				[
					'nome' => 'FNE Verde 7.25',
					'taxa_juros_aa' => 7.25,
					'taxa_basica' => 7.25,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 2
				],
				// MATO GROSSO
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 3
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 3
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 3
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 3
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 3
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 3
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 3
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 3
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 3
				],
				[
					'nome' => 'FCO Verde 8.53',
					'taxa_juros_aa' => 8.53,
					'taxa_basica' => 8.53,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 3
				],
				[
					'nome' => 'FCO Verde 7.25',
					'taxa_juros_aa' => 7.25,
					'taxa_basica' => 7.25,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 3
				],
				// MATO GROSSO DO SUL
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 4
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 4
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 4
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 4
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 4
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 4
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 4
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 4
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 4
				],
				[
					'nome' => 'FCO Verde 8.53',
					'taxa_juros_aa' => 8.53,
					'taxa_basica' => 8.53,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 4
				],
				[
					'nome' => 'FCO Verde 7.25',
					'taxa_juros_aa' => 7.25,
					'taxa_basica' => 7.25,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 4
				],
				// MINAS GERAIS
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 5
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 5
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 5
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 5
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 5
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 5
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 5
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 5
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 5
				],
				// PARÁ
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 6
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 6
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 6
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 6
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 6
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 6
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 6
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 6
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 6
				],
				[
					'nome' => 'FNO Bio 7.65 A',
					'taxa_juros_aa' => 7.65,
					'taxa_basica' => 7.65,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 6
				],
				[
					'nome' => 'FNO Bio 8.53 A',
					'taxa_juros_aa' => 8.53,
					'taxa_basica' => 8.53,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 6
				],
				[
					'nome' => 'FNO Bio 10 A',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 10.00,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 6
				],
				[
					'nome' => 'FNO Bio 7.65 B',
					'taxa_juros_aa' => 7.65,
					'taxa_basica' => 7.65,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 10,
					'carencia' => 6,
					'estado_id' => 6
				],
				[
					'nome' => 'FNO Bio 8.53 B',
					'taxa_juros_aa' => 8.53,
					'taxa_basica' => 8.53,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 10,
					'carencia' => 6,
					'estado_id' => 6
				],
				[
					'nome' => 'FNO Bio 10 B',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 10.00,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 10,
					'carencia' => 6,
					'estado_id' => 6
				],
				// PARANÁ
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 7
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 7
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 7
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 7
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 7
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 7
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 7
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 7
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 7
				],
				// PIAUÍ
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 8
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 8
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 8
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 8
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 8
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 8
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 8
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 8
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 8
				],
				[
					'nome' => 'FNE Verde 8.53',
					'taxa_juros_aa' => 8.53,
					'taxa_basica' => 8.53,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 8
				],
				[
					'nome' => 'FNE Verde 7.25',
					'taxa_juros_aa' => 7.25,
					'taxa_basica' => 7.25,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 8
				],
				// SÃO PAULO
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 9
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 9
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 9
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 9
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 9
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 9
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 9
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 9
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 9
				],
				// TOCANTINS
				[
					'nome' => 'PRONAF Floresta',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 10
				],
				[
					'nome' => 'PRONAF ECO 2.5',
					'taxa_juros_aa' => 2.50,
					'taxa_basica' => 2.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 10
				],
				[
					'nome' => 'PRONAF ECO 4.5',
					'taxa_juros_aa' => 4.50,
					'taxa_basica' => 4.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 10
				],
				[
					'nome' => 'PRONAF ECO 5.5',
					'taxa_juros_aa' => 5.50,
					'taxa_basica' => 5.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 10,
					'carencia' => 3,
					'estado_id' => 10
				],
				[
					'nome' => 'BNDES Ambiental',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 1.50,
					'risco_credito' => 1.00,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 10
				],
				[
					'nome' => 'ABC Floresta',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 10
				],
				[
					'nome' => 'ABC Floresta Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 12,
					'carencia' => 8,
					'estado_id' => 10
				],
				[
					'nome' => 'ABC Ambiental',
					'taxa_juros_aa' => 8.00,
					'taxa_basica' => 8.00,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 10
				],
				[
					'nome' => 'ABC Ambiental Pronamp',
					'taxa_juros_aa' => 7.50,
					'taxa_basica' => 7.50,
					'remuneracao_banco' => 0.0,
					'risco_credito' => 0.0,
					'prazo' => 15,
					'carencia' => 1,
					'estado_id' => 10
				],
				[
					'nome' => 'FNO Bio 7.65 A',
					'taxa_juros_aa' => 7.65,
					'taxa_basica' => 7.65,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 10
				],
				[
					'nome' => 'FNO Bio 8.53 A',
					'taxa_juros_aa' => 8.53,
					'taxa_basica' => 8.53,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 10
				],
				[
					'nome' => 'FNO Bio 10 A',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 10.00,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 20,
					'carencia' => 12,
					'estado_id' => 10
				],
				[
					'nome' => 'FNO Bio 7.65 B',
					'taxa_juros_aa' => 7.65,
					'taxa_basica' => 7.65,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 10,
					'carencia' => 6,
					'estado_id' => 10
				],
				[
					'nome' => 'FNO Bio 8.53 B',
					'taxa_juros_aa' => 8.53,
					'taxa_basica' => 8.53,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 10,
					'carencia' => 6,
					'estado_id' => 10
				],
				[
					'nome' => 'FNO Bio 10 B',
					'taxa_juros_aa' => 10.00,
					'taxa_basica' => 10.00,
					'remuneracao_banco' => 0,
					'risco_credito' => 0,
					'prazo' => 10,
					'carencia' => 6,
					'estado_id' => 10
				]
			]
		);
    }
}
