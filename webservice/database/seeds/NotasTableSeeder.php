<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class NotasTableSeeder extends Seeder
{
	/**
	* Run the database seeds.
	*
	* @return void
	*/
	public function run()
    {
		DB::table('notas')->insert(
			[
				[
					'secao' => 'titulo-home',
					'descricao' => 'Qual é o projeto de regularização que gera menor impacto financeiro para a minha atividade?'
				],
				[
					'secao' => 'descricao-home',
					'descricao' => 'Calcule a rentabilidade atual da atividade de sua propriedade e como ela será impactada ao adotar diferentes modelos de regularização ambiental para Áreas de Preservação Ambiental (APPs) e Reserva Legal (RL), utilizando capital próprio ou tomando financiamento.'
				],
				[
					'secao' => 'titulo-modal-home',
					'descricao' => 'Para obter os resultados da calculadora, tenha em mãos:'
				],
				[
					'secao' => 'descricao-modal-home',
					'descricao' => '<p>Para obter os resultados da calculadora, tenha em mãos:</p><p>- Os dados de APP e RL da propriedade (essas informações estão no CAR da propriedade);<br>- Os dados do atual uso do solo (atividade produtiva de 1ª e 2ª safra) onde ocorreria a conversão (revegetação) para a regularização ambiental;<br>- Os custos de financiamentos (de custeio e investimento).<br>- Em caso de dúvidas, utilize o botão ? presente ao lado de cada caixa em que o preenchimento é necessário;<br>- Nos casos de custos de produção de 1ª e 2ª safra, é possível utilizar dados do próprio sistema, baseados na tabela da Companhia Nacional de Abastecimento (Conab);<br>- Verifique no campo Praça se a sua região está contemplada na tabela da Conab. Em caso negativo, o preenchimento deve ser feito, mesmo com dados aproximados.</p>'
				],
				[
					'secao' => 'titulo-input-1',
					'descricao' => 'Área para regularização'
				],
				[
					'secao' => 'titulo-input-2',
					'descricao' => 'Dados da propriedade'
				],
				[
					'secao' => 'titulo-input-3',
					'descricao' => 'Atividade usada para a regularização'
				],
				[
					'secao' => 'descricao-input-3',
					'descricao' => 'As informações abaixo devem ser preenchidas para a atividade produtiva que cederá área para a regularização. Mesmo que a propriedade tenha mais de uma atividade, deve ser selecionada apenas uma nessa simulação'
				],
				[
					'secao' => 'titulo-input-4',
					'descricao' => 'Atividade usada para a regularização'
				],
				[
					'secao' => 'pergunta-input-4',
					'descricao' => 'Você realiza plantio de 2º Safra?'
				],
				[
					'secao' => 'pergunta-2-input-4',
					'descricao' => 'Qual a cultura de 2ª safra?'
				],
				[
					'secao' => 'descricao-input-4',
					'descricao' => 'Para realizar a simulação dos custos da regularização ambiental, é preciso saber se a propriedade realiza, na área que será revegetada, o plantio de segunda safra'
				],
				[
					'secao' => 'titulo-input-5',
					'descricao' => 'Custos Financeiros'
				],
				[
					'secao' => 'estado',
					'descricao' => 'Estado brasileiro onde se encontra a propriedade rural'
				],
				[
					'secao' => 'biomas',
					'descricao' => 'Bioma (conjunto de vida vegetal e animal com condições de geologia, clima e paisagens semelhantes) em que a propriedade está presente'
				],
				[
					'secao' => 'atividades_principais',
					'descricao' => 'Atividade (agropecuária) na qual ocorrerá a revegetação'
				],
				[
					'secao' => 'deficit_rl',
					'descricao' => 'Área (em hectares) que falta para você cumprir sua Reserva Legal'
				],
				[
					'secao' => 'deficit_app',
					'descricao' => 'Área (em hectares) de APP que ainda precisará ser revegetada (atualmente com uso agropecuário) na sua propriedade'
				],
				[
					'secao' => 'area_marginal',
					'descricao' => 'Área da propriedade (em hectares), sem uso'
				],
				[
					'secao' => 'app_atual_conservada',
					'descricao' => 'Área de Preservação Ambiental (em hectares) que existe hoje na propriedade e é totalmente conservada'
				],
				[
					'secao' => 'area_produtiva',
					'descricao' => 'Área total da atividade na qual  ocorrerá a regularização (em hectares)'
				],
				[
					'secao' => 'area_produtiva_2',
					'descricao' => 'Área plantada com segunda safra'
				],
				[
					'secao' => 'produtividade_media_3_safras',
					'descricao' => 'Caso não tenha o valor exato das últimas três safras em mãos, utilize um valor médio aproximado da última safra ou das duas últimas safras'
				],
				[
					'secao' => 'animais_vendidos',
					'descricao' => 'Sem descrição'
				],
				[
					'secao' => 'peso_medio_animais',
					'descricao' => 'Sem descrição'
				],
				[
					'secao' => 'media_arrobas_animal',
					'descricao' => 'Sem descrição'
				],
				[
					'secao' => 'custo_producao',
					'descricao' => 'Custo de produção operacional da atividade (sem depreciação, custos financeiros e tributos)'
				],
				[
					'secao' => 'custos_padrao',
					'descricao' => 'Os custos padrão são baseados no custo operacional da CONAB do ano de 2015. Nessa opção, indique em qual praça a propriedade rural está localizada ou a praça que possua um custo mais próximo ao seu'
				],
				[
					'secao' => 'preco_recebido',
					'descricao' => 'Preço médio recebido na transação de venda do produto para o primeiro comprador do sistema de comercialização, livre de impostos e frete'
				],
				[
					'secao' => 'producao_atual',
					'descricao' => 'Volume atual de sacas, toneladas ou arrobas produzidas'
				],
				[
					'secao' => 'cultura',
					'descricao' => 'Cultura plantada na entressafra'
				],
				[
					'secao' => 'juros_financiamento',
					'descricao' => 'Valor anual dos juros pagos em financiamentos de custeio e de investimento das atividades agropecuárias'
				],
				[
					'secao' => 'tributo_itr_csr_funrural',
					'descricao' => 'Valor anual dos impostos pagos pela propriedade.'
				],
				[
					'secao' => 'titulo-output-1',
					'descricao' => 'Rentabilidade média anual por hectare sem regularização em 20 anos'
				],
				[
					'secao' => 'titulo-output-1-resultado',
					'descricao' => 'Rentabilidade Média Anual em 20 anos (R$/ha)'
				],
				[
					'secao' => 'descricao-output-1',
					'descricao' => '<p>A rentabilidade média anual sem regularização no período de 20 anos é o primeiro resultado que a calculadora oferece. Para esse período é considerado um ganho de produtividade com base no crescimento médio anual observado nos últimos anos para a região.</p><p>A partir dessa rentabilidade é possível comparar o retorno financeiro da propriedade antes e após a regularização ambiental, considerando diferentes modelos e técnicas.</p><p>Este valor será apresentado nas próximas telas e será o seu ponto de partida para analisar o projeto de menor custo.</p><p>Os resultados apresentados avaliam o impacto econômico de diferentes técnicas de regularização. Para determinar o projeto de restauração mais adequado para a propriedade, outros fatores devem ser considerados (restrições legais, condições de solo, clima, etc.). Portanto, recomenda-se a avaliação de um técnico para a escolha do projeto de regularização.</p>'
				],
				[
					'secao' => 'titulo-output-2',
					'descricao' => 'Rentabilidade média anual por hectare da atividade após regularização usando capital próprio em 20 anos'
				],
				[
					'secao' => 'descricao-output-2',
					'descricao' => '<p>Este gráfico mostra, a partir dos dados oferecidos pelo produtor rural, qual será a rentabilidade média anual, em um horizonte de 20 anos, da atividade que será usada para revegetação, após a sua regularização ambiental. O gráfico compara o resultado para diversos modelos de regularização, tanto na APP como para a RL. Para isso, basta assinalar as técnicas que desejar para a regularização de APP e RL.</p><p>A rentabilidade média anual sem regularização é apresentada na tela como uma forma de nortear a leitura dos resultados gerados e a visualização do projeto que gera o menor impacto na rentabilidade da atividade.</p><p>O projeto de regularização que gera uma rentabilidade média anual mais próxima do retorno sem regularização é o que gera o menor impacto para a receita do produtor.</p>'
				],
				[
					'secao' => 'output-3',
					'descricao' => '<p><strong>Você também tem a possibilidade de tomar linhas de financiamento para realizar a regularização de sua propriedade. Verifique quais delas você pode utilizar.</strong></p>'
				],
				[
					'secao' => 'titulo-output-4',
					'descricao' => 'Rentabilidade média por hectare da atividade após regularização usando Capital Próprio e Financiamento (em 20 anos)'
				],
				[
					'secao' => 'descricao-output-4',
					'descricao' => 'Este gráfico mostra, a partir dos dados oferecidos pelo produtor rural, qual será a rentabilidade média da atividade que ele escolheu para regularizar no período de 20 anos. Para esse período é considerado um ganho de produtividade com base no crescimento médio anual observado nos últimos para a região. No cálculo da rentabilidade após a regularização, além dos custos de restauração para APP e RL e compensação para RL, foi considerado a perda de receita da área de APP e RL que será ou foi abandonada para a regularização. O produtor poderá analisar diversos resultados. Para isso, basta assinalar as técnicas que desejar para a regularização de APP e RL, assim como a linha de financiamento. Caso queira, entre todas as possibilidades oferecidas, verificar, automaticamente, o menor custo, basta clicar no botão "Calcular menor custo".'
				],
				[
					'secao' => 'descricao-output-4-b',
					'descricao' => '<p><strong>Atenção:</strong> Se você não é agricultor familiar não pode acessar as linhas do Pronaf</p>'
				],
				[
					'secao' => 'app',
					'descricao' => 'Técnica para a regularização de APP'
				],
				[
					'secao' => 'rl',
					'descricao' => 'Possibilidades de regularização para a RL'
				],
				[
					'secao' => 'linhas_financiamento',
					'descricao' => 'Linhas de financiamento disponível para a regularização ambiental. Na escolha da linha de financiamento deve ser levado em consideração as características de cada linha, apresentada na tela anterior'
				],
				[
					'secao' => 'titulo-output-5',
					'descricao' => 'Fluxo de caixa das atividades em 20 anos (mil reais)'
				],
				[
					'secao' => 'descricao-output-5',
					'descricao' => 'O gráfico mostra o fluxo de caixa ao longo de 20 anos da atividade com e sem a regularização ambiental. Esse pode ser visualizado de duas formas: utilizando capital próprio ou financiamento bancário.'
				],
				[
					'secao' => 'descricao-gerar-pdf',
					'descricao' => 'Obrigado por utilizar a plataforma Agroicone.<br>Estaremos sempre prontos para ajudá-lo'
				]
			]
		);
	}
}