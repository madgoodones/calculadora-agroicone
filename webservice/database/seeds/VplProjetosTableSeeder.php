<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class VplProjetosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vpl_projetos')->insert(
			[
				'crescimento_produtividade_principal' => 0.002,
	            'crescimento_produtividade_secundaria' => 0.002
			]
		);
    }
}
