@extends('layouts.dashboard')

@section('title', 'Administrador - ')

@section('content')
<h1 class="ls-title-intro ls-ico-home">Web Service</h1>

<p>Aqui é possível modificar algumas informações do projeto no menu lateral Editor</a>.</p>
<hr>
<h6 class="ls-title-5">Observação:</h6>
<p>Não recomendamos o manuseio dessa área sem conhecimento técnico.</p>
@endsection
