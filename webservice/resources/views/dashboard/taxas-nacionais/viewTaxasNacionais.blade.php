@extends('layouts.dashboard')

@section('title', 'Taxas Nacionais - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Taxas Nacionais</h1>

	<p>Estados disponíveis na plataforma</p>

	<vc-taxas-nacionais></vc-taxas-nacionais>
	<notifications></notifications>

@stop