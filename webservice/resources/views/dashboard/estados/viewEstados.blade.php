@extends('layouts.dashboard')

@section('title', 'Estados - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Estados</h1>

	<p>Estados disponíveis na plataforma</p>

	<vc-estados></vc-estados>
	<notifications></notifications>

@stop