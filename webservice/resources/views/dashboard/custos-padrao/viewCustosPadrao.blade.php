@extends('layouts.dashboard')

@section('title', 'Custos Padrão - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Custos Padrão</h1>

	<p>Estados disponíveis na plataforma</p>

	<vc-custos-padrao></vc-custos-padrao>
	<notifications></notifications>

@stop