@extends('layouts.dashboard')

@section('title', 'VPL do Projeto - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">VPL do Projeto</h1>

	<p>Estados disponíveis na plataforma</p>

	<vc-vpl-projetos></vc-vpl-projetos>
	<notifications></notifications>

@stop