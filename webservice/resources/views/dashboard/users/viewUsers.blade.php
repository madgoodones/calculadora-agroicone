@extends('layouts.dashboard')

@section('title', 'Usuários - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Usuários</h1>

	<p>Este é nosso boilerplate com a estrutura inicial de um projeto. Você pode <a href="http://locaweb.github.io/locawebstyle/documentacao/exemplos/">ver exemplos completos neste link</a>.</p>

	<vc-users></vc-users>
	<notifications></notifications>

@stop