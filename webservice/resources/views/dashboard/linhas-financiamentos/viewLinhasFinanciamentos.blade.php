@extends('layouts.dashboard')

@section('title', 'Linhas de Financiamento - ')

@section('content')
	<h1 class="ls-title-intro ls-ico-users">Linhas de Financiamentos</h1>
	<vc-linhas-financiamentos></vc-linhas-financiamentos>
	<notifications></notifications>
@stop