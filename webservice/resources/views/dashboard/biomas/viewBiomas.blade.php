@extends('layouts.dashboard')

@section('title', 'Biomas - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Biomas</h1>

	<p>Estados disponíveis na plataforma</p>

	<vc-biomas></vc-biomas>
	<notifications></notifications>

@stop