@extends('layouts.dashboard')

@section('title', 'Atividades Principais - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Atividades Principais</h1>

	<vc-atividades-principais></vc-atividades-principais>
	<notifications></notifications>

@stop