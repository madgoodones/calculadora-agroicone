@extends('layouts.dashboard')

@section('title', 'Custos de Restauros - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Custos de Restauros</h1>

	<p>Estados disponíveis na plataforma</p>

	<vc-custos-restauros></vc-custos-restauros>
	<notifications></notifications>

@stop