@extends('layouts.dashboard')

@section('title', 'Notas - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Notas</h1>

	<vc-notas></vc-notas>
	<notifications></notifications>

@stop