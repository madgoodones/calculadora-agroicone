@extends('layouts.dashboard')

@section('title', 'Modelos de Restauração - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Modelos de Restauração</h1>

	<p>Estados disponíveis na plataforma</p>

	<vc-modelos-restauracao></vc-modelos-restauracao>
	<notifications></notifications>

@stop