@extends('layouts.dashboard')

@section('title', 'Informações de Linhas de Financiamentos - ')

@section('content')

	<h1 class="ls-title-intro ls-ico-users">Informações de Linhas de Financiamentos</h1>

	<p>Estados disponíveis na plataforma</p>

	<vc-infos-linhas-financiamentos></vc-infos-linhas-financiamentos>
	<notifications></notifications>

@stop