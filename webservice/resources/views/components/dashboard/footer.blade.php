{{-- @if(config('app.env') == 'local')
	<script src="http://localhost:35729/livereload.js"></script>
@endif --}}
	{{-- JS --}}
	<script src="{{ url('dashboard/js/app.js') }}"></script>
	{{-- JQUERY --}}
	<script src="{{url('vendor/jquery/dist/jquery.min.js')}}"></script>
	<script src="{{url('vendor/locawebstyle/dist/javascripts/locastyle.js')}}"></script>
</body>
</html>