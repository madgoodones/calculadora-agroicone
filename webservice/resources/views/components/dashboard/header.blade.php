<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <title>@yield('title') Calculadora Agroicone</title>
    <meta charset="utf-8">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="Insira aqui a descrição da página.">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{url('dashboard/css/main.css')}}" rel="stylesheet" type="text/css">
    <style>
      .ls-main {
        height: auto !important;
      }
    </style>
    <link rel="stylesheet" type="text/css" href="//assets.locaweb.com.br/locastyle/3.10.1/stylesheets/locastyle.css">
  </head>
  <body>