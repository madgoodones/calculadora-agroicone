@include('components.dashboard.header')
<div class="ls-topbar">
  <!-- Barra de Notificações -->
  <div class="ls-notification-topbar">
    <!-- Dropdown com detalhes da conta de usuário -->
    <div data-ls-module="dropdown" class="ls-dropdown ls-user-account">
      <a href="#" class="ls-ico-user">
        {{-- <img src="./vendor/locawebstyle/dist/images/locastyle/avatar-example.jpg" alt="" /> --}}
        @if(Auth::check())
        <span class="ls-name">{{Auth::user()->name}}</span>
        <small class="ls-text-xs">({{Auth::user()->email}})</small>
        @endif
      </a>
      <nav class="ls-dropdown-nav ls-user-menu">
        <ul>
          <li><a href="{{ route('users.view') }}">Usuários</a></li>
          <li><a href="{{ route('logout') }}">Sair</a></li>
         </ul>
      </nav>
    </div>
  </div>
  <span class="ls-show-sidebar ls-ico-menu"></span>
  <!-- Nome do produto/marca com sidebar -->
    <h1 class="ls-brand-name">
      <a href="{{ route('admin') }}">
        Calculadora Agroicone
      </a>
    </h1>
</div>
<aside class="ls-sidebar">
<div class="ls-sidebar-inner">
    <a href="{{ route('admin') }}"  class="ls-go-prev"><span class="ls-text">Voltar à lista de serviços</span></a>
    <nav class="ls-menu">
      <ul>
         <li><a href="{{ route('admin') }}" class="ls-ico-dashboard" title="Dashboard">Dashboard</a></li>
         <li>
          <a href="#" class="ls-ico-edit-admin" title="Editor">Editor</a>
          <ul>
            <li><a href="{{ route('users.view') }}">Usuários</a></li>
            <li><a href="{{ route('estados.view') }}">Estados</a></li>
            <li><a href="{{ route('linhasFinanciamentos.view') }}">Linhas de Financiamentos</a></li>
            <li><a href="{{ route('biomas.view') }}">Biomas</a></li>
            <li><a href="{{ route('custosRestauros.view') }}">Custos de Restauros</a></li>
            <li><a href="{{ route('taxasNacionais.view') }}">Taxas Nacionais</a></li>
            <li><a href="{{ route('vplProjeto.view') }}">VPL do Projeto</a></li>
            <li><a href="{{ route('modelosRestauracao.view') }}">Modelos de Restauração</a></li>
            <li><a href="{{ route('custosPadrao.view') }}">Custos Padrão</a></li>
            <li><a href="{{ route('infosLinhasFinanciamentos.view') }}">Informações de Linhas de Financiamentos</a></li>
            <li><a href="{{ route('atividadesPrincipais.view') }}">Atividades Principais</a></li>
            <li><a href="{{ route('notas.view') }}">Notas</a></li>
          </ul>
        </li>
      </ul>
    </nav>
</div>
</aside>
<main class="ls-main">
  <div class="container-fluid" id="app">
    @yield('content')
  </div>
</main>
@include('components.dashboard.footer')