@include('components.dashboard.header')

<main class="page-main" id="app">
  <div class="container-fluid">
    <div class="ls-login-parent ls-lg-space" style="background-color: #fff;">
      <div class="ls-login-inner">
        <div class="ls-login-container">
          <div class="ls-login-box">
            @yield('content')
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@yield('footer')
@include('components.dashboard.footer')
