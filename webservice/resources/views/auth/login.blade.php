@extends('layouts.authentication')

@section('content')
<h1 class="ls-txt-center ls-title ls-md-margin-bottom">Administrar</h1>
<form class="ls-form" method="POST" action="{{ route('login') }}">
    {{ csrf_field() }}

    <div class="ls-label{{ $errors->has('email') ? ' ls-error' : '' }}">
        <label for="email"><b class="ls-label-text">E-mail</b></label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
        @if ($errors->has('email'))
        <small class="ls-help-message">{{ $errors->first('email') }}</small>
        @endif
    </div>

    <div class="ls-label{{ $errors->has('password') ? ' ls-error' : '' }}">
        <label for="password"><b class="ls-label-text">Senha</b></label>
        <input id="password" type="password" class="form-control" name="password" required>
        @if ($errors->has('password'))
        <small class="ls-help-message">{{ $errors->first('password') }}</small>
        @endif
    </div>

    <div class="ls-label">
        <label class="ls-label-text">
            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
            Lembrar a próxima
        </label>
    </div>

    <div class="ls-label">
        <button class="ls-btn ls-btn-primary" type="submit">Entrar</button>
        <a class="ls-btn" href="{{ route('password.request') }}">Esqueci minha senha</a>
    </div>
</form>
@endsection
