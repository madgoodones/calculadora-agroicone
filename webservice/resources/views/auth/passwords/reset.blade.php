@extends('layouts.authentication')

@section('content')
<div class="container">
<h1 class="ls-txt-center ls-title ls-md-margin-bottom">Resetar senha</h1>
<form class="ls-form" method="POST" action="{{ route('password.request') }}">
    {{ csrf_field() }}

    <input type="hidden" name="token" value="{{ $token }}">

    <div class="ls-label{{ $errors->has('email') ? ' ls-error' : '' }}">
        <label for="email"><b class="ls-label-text">E-mail</b></label>
        <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>
        @if ($errors->has('email'))
         <small class="ls-help-message">{{ $errors->first('email') }}</small>
        @endif
    </div>

    <div class="ls-label{{ $errors->has('password') ? ' ls-error' : '' }}">
        <label for="password"><b class="ls-label-text">Senha</b></label>
        <input id="password" type="password" class="form-control" name="password" required>

        @if ($errors->has('password'))
        <small class="ls-help-message">{{ $errors->first('password') }}</small>
        @endif
    </div>

    <div class="ls-label{{ $errors->has('password_confirmation') ? ' ls-error' : '' }}">
        <label for="password-confirm"><b class="ls-label-text">Confirmar senha</b></label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
        @if ($errors->has('password_confirmation'))
        <small class="ls-help-message">{{ $errors->first('password_confirmation') }}</small>
        @endif
    </div>

    <div class="ls-label">
        <button class="ls-btn ls-btn-primary" type="submit">Resetar senha</button>
    </div>
</form>
@endsection
