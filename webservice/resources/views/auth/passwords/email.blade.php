@extends('layouts.authentication')

@section('content')
<h1 class="ls-txt-center ls-title ls-md-margin-bottom">Recuperar senha</h1>
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<form class="ls-form" method="POST" action="{{ route('password.email') }}">
    {{ csrf_field() }}

    <div class="ls-label{{ $errors->has('email') ? ' ls-error' : '' }}">
        <label for="email"><b class="ls-label-text">E-mail</b></label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
        @if ($errors->has('email'))
        <small class="ls-help-message">{{ $errors->first('email') }}</small>
        @endif
    </div>

    <div class="ls-label">
        <button class="ls-btn ls-btn-primary" type="submit">Receber por e-mail</button>
        <a class="ls-btn" href="{{ route('login') }}">Voltar</a>
    </div>
</form>
@endsection
