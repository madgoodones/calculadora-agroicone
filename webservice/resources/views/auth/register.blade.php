@extends('layouts.authentication')

@section('content')
<div class="container">
<h1 class="ls-txt-center ls-title ls-md-margin-bottom">Cadastrar</h1>
<form class="form-horizontal" method="POST" action="{{ route('register-user') }}">
    {{ csrf_field() }}

    <div class="ls-label{{ $errors->has('name') ? ' ls-error' : '' }}">
        <label for="name"><b class="ls-label-text">Seu nome</b></label>
        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
        @if ($errors->has('name'))
        <small class="ls-help-message">{{ $errors->first('name') }}</small>
        @endif
    </div>

    <div class="ls-label{{ $errors->has('email') ? ' ls-error' : '' }}">
        <label for="email"><b class="ls-label-text">Seu e-mail</b></label>
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
        @if ($errors->has('email'))
        <small class="ls-help-message">{{ $errors->first('email') }}</small>
        @endif
    </div>

    <div class="ls-label{{ $errors->has('password') ? ' ls-error' : '' }}">
        <label for="password"><b class="ls-label-text">Sua senha</b></label>
        <input id="password" type="password" class="form-control" name="password" required>
        @if ($errors->has('password'))
        <small class="ls-help-message">{{ $errors->first('password') }}</small>
        @endif
    </div>

    <div class="ls-label">
        <label for="password-confirm"><b class="ls-label-text">Confirmar senha</b></label>
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
    </div>

    <div class="ls-label">
        <button type="submit" class="ls-btn ls-btn-primary">Pronto</button>
        <a class="ls-btn" href="{{ route('login') }}">Voltar</a>
    </div>
</form>
@endsection
