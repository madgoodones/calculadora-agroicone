import 'babel-polyfill';
import Vue from 'vue';
import store from './store/store';

import Messages from './components/messages/Messages.vue';
import Users from './components/users/Users.vue';
import Estados from './components/estados/Estados.vue';
import LinhasFinanciamentos from './components/linhas-financiamentos/LinhasFinanciamentos.vue';
import Biomas from './components/biomas/Biomas.vue';
import CustosRestauros from './components/custos-restauros/CustosRestauros.vue';
import TaxasNacionais from './components/taxas-nacionais/TaxasNacionais.vue';
import VplProjetos from './components/vpl-projetos/VplProjetos.vue';
import ModelosRestauracao from './components/modelos-restauracao/ModelosRestauracao.vue';
import CustosPadrao from './components/custos-padrao/CustosPadrao.vue';
import InfosLinhasFinanciamentos from './components/infos-linhas-financiamentos/InfosLinhasFinanciamentos.vue';
import AtividadesPrincipais from './components/atividades-principais/AtividadesPrincipais.vue';
import Notas from './components/notas/Notas.vue';

const app = new Vue({
	store,
	el: '#app',
	components: {
		'vc-messages': Messages,
		'vc-users' : Users,
		'vc-estados' : Estados,
		'vc-linhas-financiamentos' : LinhasFinanciamentos,
		'vc-biomas' : Biomas,
		'vc-custos-restauros' : CustosRestauros,
		'vc-taxas-nacionais' : TaxasNacionais,
		'vc-vpl-projetos' : VplProjetos,
		'vc-modelos-restauracao' : ModelosRestauracao,
		'vc-custos-padrao' : CustosPadrao,
		'vc-infos-linhas-financiamentos': InfosLinhasFinanciamentos,
		'vc-atividades-principais': AtividadesPrincipais,
		'vc-notas': Notas
	},
	created() {
		this.$notifications.setOptions({
			type: 'info', 
			timeout: 4000,
			horizontalAlign: 'right',
			verticalAlign: 'bottom'
		});
	}
});