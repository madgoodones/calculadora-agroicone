require('../bootstrap');

import Vue from 'vue';
import Vuex from 'vuex';
import Notify from 'vue-notifyjs';

import state from './state';

Vue.use(Vuex);
Vue.use(Notify);

export default new Vuex.Store({
	state
});