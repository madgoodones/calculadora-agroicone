<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Precisa ser aceito.',
    'active_url'           => 'Infelizmente não é uma URL válida.',
    'after'                => 'Precisa ser uma data depois de :date. Por favor, você pode verificar isso?',
    'after_or_equal'       => 'Precisa ser uma data posterior ou igual a :date. Por favor, você pode verificar isso?',
    'alpha'                => 'Deve conter somente letras. Por favor, você pode verificar isso?',
    'alpha_dash'           => 'Deve conter letras, números e traços. Por favor, você pode verificar isso?',
    'alpha_num'            => 'Deve conter somente letras e números. Por favor, você pode verificar isso?',
    'array'                => 'Precisa ser um array. Por favor, você pode verificar isso?',
    'before'               => 'Precisa ser uma data antes de :date. Por favor, você pode verificar isso?',
    'before_or_equal'      => 'Precisa ser uma data anterior ou igual a :date. Por favor, você pode verificar isso?',
    'between'              => [
        'numeric' => 'Ops, deve estar entre :min e :max. Por favor, você pode verificar isso?',
        'file'    => 'Ops, deve estar entre :min e :max kilobytes. Por favor, você pode verificar isso?',
        'string'  => 'Ops, deve estar entre :min e :max caracteres. Por favor, você pode verificar isso?',
        'array'   => 'Ops, precisa ter entre :min e :max itens. Por favor, você pode verificar isso?',
    ],
    'boolean'              => 'Precisa ser verdadeiro ou falso. Por favor, você pode verificar isso?',
    'confirmed'            => 'A confirmação de não confere. Por favor, você pode verificar isso?',
    'date'                 => 'Infelizmente não é uma data válida. Por favor, você pode verificar isso?',
    'date_format'          => 'Infelizmente não confere com o formato :format. Por favor, você pode verificar isso?',
    'different'            => 'E :other devem ser diferentes. Por favor, você pode verificar isso?',
    'digits'               => 'Precisa ter :digits dígitos. Por favor, você pode verificar isso?',
    'digits_between'       => 'Precisa ter entre :min e :max dígitos. Por favor, você pode verificar isso?',
    'dimensions'           => 'Tem dimensões de imagem inválidas. Por favor, você pode verificar isso?',
    'distinct'             => 'Tem um valor duplicado. Por favor, você pode verificar isso?',
    'email'                => 'Precisa ser um endereço de e-mail válido. Por favor, você pode verificar isso?',
    'exists'               => 'Selecionado é inválido. Por favor, você pode verificar isso?',
    'file'                 => 'Precisa ser um arquivo. Por favor, você pode verificar isso?',
    'filled'               => 'É um campo requerido. Por favor, você pode verificar isso?',
    'image'                => 'Precisa ser uma imagem. Por favor, você pode verificar isso?',
    'in'                   => 'É inválido. Por favor, você pode verificar isso?',
    'in_array'             => 'não existe em :other. Por favor, você pode verificar isso?',
    'integer'              => 'Precisa ser um inteiro. Por favor, você pode verificar isso?',
    'ip'                   => 'Precisa ser um endereço IP válido. Por favor, você pode verificar isso?',
    'json'                 => 'Precisa ser um JSON válido. Por favor, você pode verificar isso?',
    'max'                  => [
        'numeric' => 'Ops, não precisa ser maior que :max. Por favor, você pode verificar isso?',
        'file'    => 'Ops, não precisa ter mais que :max kilobytes. Por favor, você pode verificar isso?',
        'string'  => 'Ops, não precisa ter mais que :max caracteres. Por favor, você pode verificar isso?',
        'array'   => 'Ops, não precisa ter mais que :max itens. Por favor, você pode verificar isso?',
    ],
    'mimes'                => 'Precisa ser um arquivo do tipo: :values. Por favor, você pode verificar isso?',
    'mimetypes'            => 'Precisa ser um arquivo do tipo: :values. Por favor, você pode verificar isso?',
    'min'                  => [
        'numeric' => 'Precisa ser no mínimo :min. Por favor, você pode verificar isso?',
        'file'    => 'Precisa ter no mínimo :min kilobytes. Por favor, você pode verificar isso?',
        'string'  => 'Precisa ter no mínimo :min caracteres. Por favor, você pode verificar isso?',
        'array'   => 'Precisa ter no mínimo :min itens. Por favor, você pode verificar isso?',
    ],
    'not_in'               => 'O selecionado é inválido. Por favor, você pode verificar isso?',
    'numeric'              => 'Precisa ser um número. Por favor, você pode verificar isso?',
    'present'              => 'O campo precisa ser presente. Por favor, você pode verificar isso?',
    'regex'                => 'O formato de é inválido. Por favor, você pode verificar isso?',
    'required'             => 'O campo precisa ser informado. Por favor, você pode verificar isso?',
    'required_if'          => 'O campo precisa ser informado quando :other é :value. Por favor, você pode verificar isso?',
    'required_unless'      => 'O é necessário a menos que :other esteja em :values. Por favor, você pode verificar isso?',
    'required_with'        => 'O campo precisa ser informado quando :values está presente. Por favor, você pode verificar isso?',
    'required_with_all'    => 'O campo precisa ser informado quando :values estão presentes. Por favor, você pode verificar isso?',
    'required_without'     => 'O campo precisa ser informado quando :values não está presente. Por favor, você pode verificar isso?',
    'required_without_all' => 'O campo precisa ser informado quando nenhum destes estão presentes: :values. Por favor, você pode verificar isso?',
    'same'                 => 'e :other devem ser iguais.',
    'size'                 => [
        'numeric' => 'Ops, precisa ser :size. Por favor, você pode verificar isso?',
        'file'    => 'Ops, precisa ter :size kilobytes. Por favor, você pode verificar isso?',
        'string'  => 'Ops, precisa ter :size caracteres. Por favor, você pode verificar isso?',
        'array'   => 'Ops, deve conter :size itens. Por favor, você pode verificar isso?',
    ],
    'string'               => 'Ops, precisa ser uma string.',
    'timezone'             => 'Ops, precisa ser uma timezone válida.',
    'unique'               => 'Ops, já está em uso.',
    'uploaded'             => 'Ops, falhou ao ser enviado.',
    'url'                  => 'O formato de é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
