const { mix } = require('laravel-mix');
let vendor_path = 'public/vendor/',
LiveReloadPlugin = require('webpack-livereload-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Mix Options
mix.options({
  extractVueStyles: false,
  processCssUrls: true,
  uglify: {
    compress: true,
    comments: false,
    sourceMap: true
  },
  purifyCss: false,
  postCss: [
  		require('autoprefixer')({grid: false}),
		require('cssnano')(),
		require('postcss-discard-comments')({removeAll: true})
  ],
  clearConsole: false
});
mix.webpackConfig({
    plugins: [
        new LiveReloadPlugin()
    ]
});

// Compile Stylus
let stylusArguments = {
		use: [
			require('kouto-swiss')(),
			require('rupture')(),
			require('jeet')()
		],
		'resolve url': true,
		define: {
			url: require('stylus').resolver()
		}
	},
	stylusOptions = {
		postCss: [
			require('autoprefixer')({grid: false}),
			require('cssnano')(),
			require('postcss-discard-comments')({removeAll: true})
		]
	};

// Archives to Dashboard
mix.stylus('resources/assets/dashboard/stylus/main.styl', 'public/dashboard/css/main.css', stylusArguments).version(); // Stylus
mix.js('resources/assets/dashboard/js/app.js', 'public/dashboard/js/').version(); // VueJS

// Browser Sync
mix.browserSync('localhost:8000');
