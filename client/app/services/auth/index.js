import axios from 'axios';

export default {

  auth(email, password) {
    return axios.post('auth-app', {
    	email: email,
    	password: password
    });
  }

}
