export default {

	'CHANGE_ESTADO' (state, payload) {
		state.premissas_regularizacao.estado = payload;
	},

	'CHANGE_BIOMA' (state, payload) {
		state.premissas_regularizacao.bioma = payload;
	},
	
	'CHANGE_ATIVIDADE_PRINCIPAL' (state, payload) {
		state.premissas_regularizacao.atividade = payload;
	},

	'UPDATE_NOTAS' (state, payload) {
		state.notas = payload;
	},

	'UPDATE_DEFICIT_RL' (state, payload) {
		state.dados_propriedade.deficit_rl = payload;
	},

	'UPDATE_DEFICIT_APP' (state, payload) {
		state.dados_propriedade.deficit_app = payload;
	},

	'UPDATE_AREA_MARGINAL' (state, payload) {
		state.dados_propriedade.area_marginal = payload;
	},

	'UPDATE_APP_ATUAL_CONSERVADA' (state, payload) {
		state.dados_propriedade.app_atual_conservada = payload;
	},

	'UPDATE_AREA_PRODUTIVA' (state, payload) {
		state.uso_atual_area_regularizacao.area_produtiva = payload;
	},

	'UPDATE_MEDIA_ARROBAS_ANIMAL' (state, payload) {
		state.uso_atual_area_regularizacao.media_arrobas_animal = payload;
	},

	'UPDATE_ANIMAIS_VENDIDOS' (state, payload) {
		state.uso_atual_area_regularizacao.animais_vendidos = payload;
	},

	'UPDATE_PRODUTIVIDADE_MEDIA_3_SAFRAS' (state, payload) {
		state.uso_atual_area_regularizacao.produtividade_media_3_safras = payload;
	},

	'UPDATE_CUSTO_PRODUCAO' (state, payload) {
		state.uso_atual_area_regularizacao.custo_producao = payload;
	},

	'UPDATE_PRECO_RECEBIDO' (state, payload) {
		state.uso_atual_area_regularizacao.preco_recebido = payload;
	},

	'UPDATE_PRODUCAO_ATUAL' (state, payload) {
		state.uso_atual_area_regularizacao.producao_atual = payload;
	},

	'UPDATE_SEGUNDA_SAFRA' (state, payload) {
		state.segunda_safra_show = payload;
	},

	'UPDATE_SEGUNDA_SAFRA_CULTURA' (state, payload) {
		state.segunda_safra.cultura = payload;
	},

	'UPDATE_SEGUNDA_SAFRA_AREA_PRODUTIVA' (state, payload) {
		state.segunda_safra.area_produtiva = payload;
	},

	'UPDATE_SEGUNDA_SAFRA_PRODUTIVIDADE_MEDIA_3_SAFRAS' (state, payload) {
		state.segunda_safra.produtividade_media_3_safras = payload;
	},

	'UPDATE_SEGUNDA_SAFRA_CUSTO_PRODUCAO' (state, payload) {
		state.segunda_safra.custo_producao = payload;
	},

	'UPDATE_SEGUNDA_SAFRA_PRECO_RECEBIDO' (state, payload) {
		state.segunda_safra.preco_recebido = payload;
	},

	'UPDATE_SEGUNDA_SAFRA_PRODUCAO_ATUAL' (state, payload) {
		state.segunda_safra.producao_atual = payload;
	},

	'UPDATE_JUROS_FINANCIAMENTO' (state, payload) {
		state.custos_financeiros.juros_financiamento = payload;
	},

	'UPDATE_TRIBUTO_ITR_CSR_FUNRURAL' (state, payload) {
		state.custos_financeiros.tributo_itr_csr_funrural = payload;
	},

	'UPDATE_RESULTADOS' (state, payload) {
		state.resultados = payload;
	},

	'UPDATE_REGULARIZACAO_BARATA' (state, payload) {
		state.resultados.regularizacao_barata = payload;
	},

	'UPDATE_SHOW_CALCULANDO' (state, payload) {
		state.showCalculando = payload;
	},

	'UPDATE_SHOW_RESULTADOS' (state, payload) {
		state.showResultados = payload;
	},

	'UPDATE_APP' (state, payload) {
		state.app = payload;
	},

	'UPDATE_RESERVA_LEGAL' (state, payload) {
		state.reserva_legal = payload;
	},

	'UPDATE_LINHA_FINANCIAMENTO' (state, payload) {
		state.linha_financiamento = payload;
	},

	'UPDATE_GENERATE_PDF' (state, payload) {
		state.generatePDF = payload;
	},

	'UPDATE_GRAFICO_CAPITAL_PROPRIO' (state, payload) {
		state.graficoCapitalProprio = payload;
	},

	'UPDATE_GRAFICO_20_ANOS_SIMULACAO' (state, payload) {
		state.grafico20AnosSimulacao = payload;
	},

	'UPDATE_GRAFICO_FLUXO_CAPITAL_PROPRIO' (state, payload) {
		state.graficoFluxoCapitalProprio = payload;
	},

	'UPDATE_GRAFICO_FLUXO_FINANCIAMENTO' (state, payload) {
		state.graficoFluxoFinanciamento = payload;
	},
}