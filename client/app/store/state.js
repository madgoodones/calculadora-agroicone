export default {
	showCalculando: false,
	showResultados: false,
	fullpageOptions: {
		menu: '#sections-menu',
		scrollBar: true,
		responsiveWidth: 1105,
		onLeave: function(index, nextIndex, direction) {
			if(nextIndex == 3){
				$('.nav-helper-text').fadeOut();
			}
		}
	},
	notas: [],
	premissas_regularizacao: {
		estado: 1,
		bioma: 1,
		atividade: 1
	},
	dados_propriedade: {
		deficit_rl: '',
		deficit_app: '',
		area_marginal: '',
		app_atual_conservada: ''
	},
	uso_atual_area_regularizacao: {
		area_produtiva: '',
		produtividade_media_3_safras: '',
		custo_producao: '',
		preco_recebido: '',
		producao_atual: '',
		media_arrobas_animal: '',
		animais_vendidos: ''
	},
	segunda_safra: {
		cultura: 0,
		area_produtiva: '',
		produtividade_media_3_safras: '',
		custo_producao: '',
		preco_recebido: '',
		producao_atual: ''
	},
	custos_financeiros: {
		juros_financiamento: '',
		tributo_itr_csr_funrural: ''
	},
	resultados: false,
	app: '',
	reserva_legal: '',
	linha_financiamento: 'pronaf-floresta',
	generatePDF: false,
	graficoCapitalProprio: '',
	grafico20AnosSimulacao: '',
	graficoFluxoCapitalProprio: '',
	graficoFluxoFinanciamento: '',
	highchartsThemeExport: {
		colors: [
			'rgb(176,158,117)',
			'rgb(109,178,134)',
			'rgb(123,127,140)',
			'rgb(176, 110, 109)'
		],
		chart: {
			type: 'column',
			width: 1000
		},
		title: {
			text: ''
		},
		yAxis: {
			labels: {
				enabled: false
			},
			title: {
				enabled: false
			}
		},
		xAxis: {
			categories: []  
		},
		plotOptions: {
		  column: {
		    dataLabels: {
		      enabled: true,
		      format: 'R$ {point.y:,.0f}',
		      style: {
		        fontSize: '16px'
		      }
		    }
		  }
		},
		series: [],
		exporting: {
			width: 1000
		},
		credits: {
			enabled: false
		}
	}
}