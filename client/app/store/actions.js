export default {

	'ChangeEstado' (context, payload) {
		context.commit('CHANGE_ESTADO', payload);
	},

	'ChangeBioma' (context, payload) {
		context.commit('CHANGE_BIOMA', payload);
	},
	
	'ChangeAtividadePrincipal' (context, payload) {
		context.commit('CHANGE_ATIVIDADE_PRINCIPAL', payload);
	},

	'UpdateNotas' (context, payload) {
		context.commit('UPDATE_NOTAS', payload);
	},

	'updateDeficitRl' (context, payload) {
		context.commit('UPDATE_DEFICIT_RL', payload);
	},
	
	'updateDeficitApp' (context, payload) {
		context.commit('UPDATE_DEFICIT_APP', payload);
	},

	'updateAreaMarginal' (context, payload) {
		context.commit('UPDATE_AREA_MARGINAL', payload);
	},

	'updateAppAtualConservada' (context, payload) {
		context.commit('UPDATE_APP_ATUAL_CONSERVADA', payload);
	},

	'updateAreaProdutiva' (context, payload) {
		context.commit('UPDATE_AREA_PRODUTIVA', payload);
	},

	'updateProdutividadeMedia3Safras' (context, payload) {
		context.commit('UPDATE_PRODUTIVIDADE_MEDIA_3_SAFRAS', payload);
	},

	'updateCustoProducao' (context, payload) {
		context.commit('UPDATE_CUSTO_PRODUCAO', payload);
	},

	'updatePrecoRecebido' (context, payload) {
		context.commit('UPDATE_PRECO_RECEBIDO', payload);
	},

	'updateProducaoAtual' (context, payload) {
		context.commit('UPDATE_PRODUCAO_ATUAL', payload);
	},

	'updateMediaArrobasAnimal' (context, payload) {
		context.commit('UPDATE_MEDIA_ARROBAS_ANIMAL', payload);
	},

	'updateAnimaisVendidos' (context, payload) {
		context.commit('UPDATE_ANIMAIS_VENDIDOS', payload);
	},

	'updateSegundaSafra' (context, payload) {
		context.commit('UPDATE_SEGUNDA_SAFRA', payload);
	},

	'updateSegundaSafraCultura' (context, payload) {
		context.commit('UPDATE_SEGUNDA_SAFRA_CULTURA', payload);
	},

	'updateSegundaSafraAreaProdutiva' (context, payload) {
		context.commit('UPDATE_SEGUNDA_SAFRA_AREA_PRODUTIVA', payload);
	},

	'updateSegundaSafraProdutividadeMedia3Safras' (context, payload) {
		context.commit('UPDATE_SEGUNDA_SAFRA_PRODUTIVIDADE_MEDIA_3_SAFRAS', payload);
	},

	'updateSegundaSafraCustoProducao' (context, payload) {
		context.commit('UPDATE_SEGUNDA_SAFRA_CUSTO_PRODUCAO', payload);
	},

	'updateSegundaSafraPrecoRecebido' (context, payload) {
		context.commit('UPDATE_SEGUNDA_SAFRA_PRECO_RECEBIDO', payload);
	},

	'updateSegundaSafraProducaoAtual' (context, payload) {
		context.commit('UPDATE_SEGUNDA_SAFRA_PRODUCAO_ATUAL', payload);
	},

	'updateJurosFinanciamento' (context, payload) {
		context.commit('UPDATE_JUROS_FINANCIAMENTO', payload);
	},

	'updateTributoItrCsrFunrural' (context, payload) {
		context.commit('UPDATE_TRIBUTO_ITR_CSR_FUNRURAL', payload);
	},

	'updateResultados' (context, payload) {
		context.commit('UPDATE_RESULTADOS', payload);
	},
	
	'updateRegularizacaoBarata' (context, payload) {
		context.commit('UPDATE_REGULARIZACAO_BARATA', payload);
	},

	'updateShowCalculando' (context, payload) {
		context.commit('UPDATE_SHOW_CALCULANDO', payload);
	},

	'updateShowResultados' (context, payload) {
		context.commit('UPDATE_SHOW_RESULTADOS', payload);
	},
	'updateApp' (context, payload) {
		context.commit('UPDATE_APP', payload);
	},
	'updateReservaLegal' (context, payload) {
		context.commit('UPDATE_RESERVA_LEGAL', payload);
	},
	'updateLinhaFinanciamento' (context, payload) {
		context.commit('UPDATE_LINHA_FINANCIAMENTO', payload);
	},
	'updateGeneratePDF' (context, payload) {
		context.commit('UPDATE_GENERATE_PDF', payload);
	},
	'updateGraficoCapitalProprio' (context, payload) {
		context.commit('UPDATE_GRAFICO_CAPITAL_PROPRIO', payload);
	},
	'updateGrafico20AnosSimulacao' (context, payload) {
		context.commit('UPDATE_GRAFICO_20_ANOS_SIMULACAO', payload);
	},
	'updateGraficoFluxoCapitalProprio' (context, payload) {
		context.commit('UPDATE_GRAFICO_FLUXO_CAPITAL_PROPRIO', payload);
	},
	'updateGraficoFluxoFinanciamento' (context, payload) {
		context.commit('UPDATE_GRAFICO_FLUXO_FINANCIAMENTO', payload);
	}
}