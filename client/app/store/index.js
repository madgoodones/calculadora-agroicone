import Vue from 'vue';
import Vuex from 'vuex';
import Session from 'vue-session';
import Notify from 'vue-notifyjs';
import VueCurrencyFilter from 'vue-currency-filter';
import VueHighcharts from 'vue-highcharts';
import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting';
import VueNumeric from 'vue-numeric';

import state from './state';
import mutations from './mutations';
import actions from './actions';

import highchartsTheme from '../assets/js/highcharts.theme.js';
import highchartsLang from '../assets/js/highcharts.lang.js';

Highcharts.theme = highchartsTheme;

Exporting(Highcharts);
Highcharts.setOptions(Highcharts.theme);
Highcharts.setOptions(highchartsLang);

Vue.use(Vuex);
Vue.use(Session);
Vue.use(Notify);
Vue.use(VueCurrencyFilter, {
  symbol : 'R$', 
  thousandsSeparator: '.',
  fractionCount: 2,
  fractionSeparator: ',',
  symbolPosition: 'front',
  symbolSpacing: true
});
Vue.use(VueHighcharts, { Highcharts });
Vue.use(VueNumeric);

export default new Vuex.Store({
	state,
	actions,
	mutations
});