import Vue from 'vue'
import Router from 'vue-router'

import App from '../components/App.vue'
import Home from '../components/home/Home.vue'
import Login from '../components/auth/Login.vue'

Vue.use(Router);

let routes = [
	{
		path: '*',
		redirect: 'login'
	},
	{
		path: '/login',
		name: 'login',
		component: Login
	},
	{
		path: '/',
		name: 'home',
		component: Home
	},
	{
		path: '/dashboard',
		name: 'dashboard',
		component: App
	}
];

const router = new Router({
	routes: routes,
	mode: 'history',
	base: '/',
	linkActiveClass: 'active-section',
	linkExactActiveClass: 'current-section'
})

export default router