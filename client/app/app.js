import './bootstrap'
import Vue from 'vue';
import store from './store'
import router from './router'

// Components
import Wrapper from './components/Wrapper.vue'

Vue.config.devtools = false
Vue.config.debug = false
Vue.config.silent = true

new Vue({
	store,
	router: router,
	el: '#app',
	render: h => h(Wrapper)
})
