const path    = require('path')
const webpack = require('webpack')

const CleanWebpackPlugin = require('clean-webpack-plugin')
const ExtractTextPlugin  = require('extract-text-webpack-plugin')

const autoprefixer = require('autoprefixer')
const cssnano      = require('cssnano')

// Copy and Minify images
const ImageminPlugin = require('imagemin-webpack-plugin').default
const CopyWebpackPlugin = require('copy-webpack-plugin')

let isProd = (process.env.NODE_ENV === 'production')
let cssDev = (isProd ? '' : `?root=${__dirname}&sourceMap`)

module.exports = {
  context: __dirname,
  entry: './app/app.js',
  output: {
    publicPath: '/',
    filename: './assets/js/bundle.js'
  },
  resolveLoader: {
    root: path.join(__dirname, 'node_modules'),
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.html$/,
        loader: 'vue-html'
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract('style', `css${cssDev}!postcss`)
      },
      {
        test: /\.styl$/,
        loader: ExtractTextPlugin.extract('style', `css${cssDev}!postcss!stylus`)
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file?name=assets/img/[name].[ext]?[hash]'
      },
      { test: /\.svg$/, loader: 'url?limit=65000&mimetype=image/svg+xml&name=assets/fonts/[name].[ext]' },
      { test: /\.woff$/, loader: 'url?limit=65000&mimetype=application/font-woff&name=assets/fonts/[name].[ext]' },
      { test: /\.woff2$/, loader: 'url?limit=65000&mimetype=application/font-woff2&name=assets/fonts/[name].[ext]' },
      { test: /\.[ot]tf$/, loader: 'url?limit=65000&mimetype=application/octet-stream&name=assets/fonts/[name].[ext]' },
      { test: /\.eot$/, loader: 'url?limit=65000&mimetype=application/vnd.ms-fontobject&name=assets/fonts/[name].[ext]' }
    ]
  },
  stylus: {
    use: [
      require('rupture')(),
      require('jeet')(),
      require('kouto-swiss')()
    ],
    'resolve url': true,
    define: {
      url: require('stylus').resolver()
    }
  },
  postcss: function() {
    return [
      autoprefixer({ grid: false }),
      cssnano({
        discardComments: {
          removeAll: true
        }
      })
    ]
  },
  plugins: [
    new CleanWebpackPlugin('assets'),
    new ExtractTextPlugin('assets/css/styles.css', { 
      disable: !isProd
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      output: {
        comments: false,
      },
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery"
    })
  ],
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './app'),
      vue: 'vue/dist/vue.js'
    }
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true
  },
  devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin(),
    new CopyWebpackPlugin([{
      from: 'app/assets/img/', to: 'assets/img/'
    }]),
    new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i })
  ])
}
